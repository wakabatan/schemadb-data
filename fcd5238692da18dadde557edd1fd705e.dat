<!doctype html>
<html lang='en'>
<head>
<meta charset="utf-8">
<title>Browser Testing and Tools Working Group</title>
<link rel="stylesheet" href="/2005/10/w3cdoc.css" type="text/css" media="screen">
<link rel="stylesheet" type="text/css" href="/Guide/pubrules-style.css">
<link rel="stylesheet" type="text/css" href="/2006/02/charter-style.css">
<style>
  .draft {
    color: red;
  }
</style>
</head>
  <body>
    <div id="template">
      <ul id="navbar" style="font-size: small">
        <li><a href="#scope">Scope</a></li>
        <li><a href="#deliverables">Deliverables</a></li>
        <li><a href="#coordination">Dependencies and Liaisons</a></li>
        <li><a href="#participation">Participation</a></li>
        <li><a href="#communication">Communication</a></li>
        <li><a href="#decisions">Decision Policy</a></li>
        <li><a href="#patentpolicy">Patent Policy</a></li>
        <li><a href="#about">About this Charter</a></li>
      </ul>
      <p>
        <a href="/"><img alt="W3C" height="48" src="/Icons/w3c_home" width="72" /></a>
        <a class="domainlogo" href="/Interaction/"><img src="/Icons/interaction" alt="Interaction Domain"></a>
      </p>
      <h1 id="title">Charter: Browser Testing and Tools Working Group</h1>

      <p class="mission">
      The <strong>mission</strong> of the
      <a href='/testing/browser/'>Browser Testing and Tools Working Group</a>,
      part of the
      <a href="/2011/05/activity-proposal.html">Testing Activity</a>,
      is to produce technologies for use in testing, debugging, and
      troubleshooting of Web applications running in Web browsers.</p>
      <div class="noprint">
        <p class="join">
          Join the <a href='http://www.w3.org/2004/01/pp-impl/49799/join'>Browser Testing and Tools Working Group.</a>
        </p>
      </div>
      <table class="summary-table">
        <tr id="Duration">
          <th rowspan="1" colspan="1">End date</th>
          <td rowspan="1" colspan="1">31 December 2013</td>
        </tr>
        <tr>
          <th rowspan="1" colspan="1">Initial Chair</th>
          <td rowspan="1" colspan="1">Wilhelm Joys Andersen</td>
        </tr>
        <tr>
          <th rowspan="1" colspan="1">Initial Team Contact<br>(FTE %: 10)</th>
          <td rowspan="1" colspan="1">Michael[tm] Smith</td>
        </tr>
        <tr>
        <th rowspan="1" colspan="1">Usual Meeting Schedule</th>
          <td rowspan="1" colspan="1">
            <div>IRC-only meetings: On an as-needed basis, up to once per week.</div>
            <div>Teleconferences: On an as-needed basis, up to once a week.</div>
            <div>Face-to-face meetings: On an as-needed basis, up to twice per year.</div>
          </td>
        </tr>
      </table>
      <div class="scope">
        <h2 id="scope">Scope</h2>
        <p>The scope of the Browser Testing and Tools Working Group
        includes:</p>
        <ul>
          <li>APIs (application programming interfaces) for use in automated testing of Web applications</li>
          <li>APIs for use in troubleshooting and debugging of Web applications</li>
        </ul>
        <h3>APIs for use in automated testing of Web applications</h3>
        <p>For the purpose of automating testing of Web applications
        running in browsers, there is a specific need to simulate user
        actions such as clicking links, entering text, and submitting
        forms.
        The Browser Testing and Tools Working Group will produce a
        specification for a “Web Driver” API to meet that need.</p>

        <h3>APIs for use in troubleshooting and debugging of Web applications</h3>
     
        <p>For the purpose of troubleshooting and debugging Web
        applications, many Web browsers provide either a built-in set of
        “developer tools” or have a set of such tools that are available as
        a plug-in.  Examples include:</p>
        <ul>
         <li>the
         <a href="http://getfirebug.com/">Firebug</a>
         plugin for Mozilla Firefox</li>
         <li>Opera
         <a href="http://www.opera.com/dragonfly/">Dragonfly</a></li>
         <li>the Microsoft Internet Explorer
         <a href="http://msdn.microsoft.com/en-us/ie/aa740478.aspx">Developer Tools</a>
         feature</li>
         <li>WebKit and Apple Safari
         <a href="http://www.apple.com/safari/features.html#developer">Web Inspector</a></li>
         <li>the Google Chrome
         <a href="http://www.chromium.org/devtools">Developer Tools</a>
         feature</li>
        </ul>
        <p>The purposes of these developer tools include:</p>
        <ul>
          <li>inspecting and modifying the DOM for a particular document/application</li>
          <li>examining and editing associated CSS properties</li>
          <li>monitoring and analyzing network activity, HTTP request and
            response headers, XHR (XMLHttpRequest) details, RAM usage, HTTP caching,
            and persistent data storage</li>
          <li>viewing logs of errors, warnings, and informational messages
           emitted by any JavaScript code that accompanies the
           document/application, and by the JavaScript interpreter that executes
           that code, as well as by other parts of the overall browser engine
           (such as the DOM parser)</li>
          <li>analyzing and debugging of any accompanying JavaScript code that
            accompanies the document/application; more specifically:
            <ul>
              <li>setting debug breakpoints, stepping through code,
              examining the current call stack and variables</li>
              <li>using a provided command-line console/shell
              interface—more precisely, a “read-eval-print loop” (REPL)—to
              perform a variety of tasks, including directly executing
              JavaScript code typed in by the user-developer</li>
              <!-- <li>remote debugging, particularly in mobile devices</li> -->
            </ul>
          </li>
          <li>profiling accompanying JavaScript code</li>
        </ul>

        <p>Those developer tools sometimes expose APIs that give
        developers programmatic access to certain parts of the
        development environment. For example, many expose
        a <code>console</code> API which, among other things, enables
        developers to programatically control logging of particular
        messages to an error console.</p>

        <p>The Browser Testing and Tools Working Group will produce a
        specification for such a “Console” API, as well as consider any
        other potential APIs useful in performing troubleshooting and
        debugging of Web applications.</p>
        
        <h3 id="success">Success Criteria</h3>
        <p>
          In order to advance to Proposed Recommendation, each
          specification developed by the Browser Testing and Tools Working
          Group is expected to have at least two independent
          implementations of every feature defined in the specification.
        </p>
      </div>

      <div>
        <h2 id="deliverables">
          Deliverables
        </h2>
        <p>
          The Browser Testing and Tools Working Group will deliver the
          following W3C Recommendations:
        </p>
        <dl>
          <dt>Web Driver API</dt>
          <dd>
            The Web Driver API provides methods that allow automated
            simulation of user actions, such as clicking links, entering
            text, and submitting forms.
          </dd>
          <dt>Console API</dt>
          <dd>
            The Console API provides methods that allow Web developers to,
            among other things, programatically write messages and data to
            a developer-tools console component. The messages include
            generic log messages, as well as error, warning, and info
            messages. The data includes stack traces and interactive “tree
            view” representations of objects. Other methods provide ways to
            start and stop timers, to clear all content from the console,
            and more.
          </dd>
        </dl>
        <div id="ig-other-deliverables">
          <h3>
            Other Deliverables
          </h3>
          <p>Other deliverables that the group may also consider
          developing include a normative specification for a protocol
          for use in remote debugging of Web applications.</p>
          <p>
            Furthermore, the group may produce any number of non-normative
            documents, such as:
          </p>
          <ul>
            <li>Use case and requirement documents</li>
            <li>Test suites for each specification</li>
            <li>Primer or Best Practice documents, or other types of
            how-to/tutorial documentation</li>
            <li>Script libraries</li>
          </ul>
        </div>
        <h3>
          Milestones
        </h3>
        <table class="roadmap">
          <caption>Milestones</caption>
          <tfoot>
            <tr>
              <td colspan="6" rowspan="1">
                Note: The group will document significant changes from this
                initial schedule on the
                <a href="/testing/browser/">group’s home page</a>.
              </td>
            </tr>
          </tfoot>
          <tbody>
            <tr>
              <th rowspan="1" colspan="1">
                Specification
              </th>
              <th rowspan="1" colspan="1">
                <abbr title="First Working Draft">FPWD</abbr>
              </th>
              <th rowspan="1" colspan="1">
                <abbr title="Last Call Working Draft">LC</abbr>
              </th>
              <th rowspan="1" colspan="1">
                <abbr title="Candidate Recommendation">CR</abbr>
              </th>
              <th rowspan="1" colspan="1">
                <abbr title="Proposed Recommendation">PR</abbr>
              </th>
              <th rowspan="1" colspan="1">
                <abbr title="Recommendation">Rec</abbr>
              </th>
            </tr>
            <tr>
              <th rowspan="1" colspan="1">
                Console Interface
              </th>
              <td class="WD1" rowspan="1" colspan="1">
                October 2011
              </td>
              <td class="LC" rowspan="1" colspan="1">
                May 2012
              </td>
              <td class="CR" rowspan="1" colspan="1">
                June 2012
              </td>
              <td class="PR" rowspan="1" colspan="1">
                Dec 2012
              </td>
              <td class="REC" rowspan="1" colspan="1">
                Feb 2013
              </td>
            </tr>
            <tr>
              <th rowspan="1" colspan="1">
                Web Driver Interface
              </th>
              <td class="WD1" rowspan="1" colspan="1">
                October 2011
              </td>
              <td class="LC" rowspan="1" colspan="1">
                May 2012
              </td>
              <td class="CR" rowspan="1" colspan="1">
                June 2012
              </td>
              <td class="PR" rowspan="1" colspan="1">
                Dec 2012
              </td>
              <td class="REC" rowspan="1" colspan="1">
                Feb 2013
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="dependencies">
        <h2 id="coordination">
          Dependencies and liaisons
        </h2>
        <h3 id="w3c-groups">W3C groups</h3>
        <ul>
          <li><a href="/Style/CSS/">CSS Working Group</a></li>
          <li><a href="/2009/dap/">Device APIs Working Group</a></li>
          <li><a href="/WAI/RC/">Evaluation and Repair Tools Working Group</a></li>
          <li><a href="/html/wg/">HTML Working Group</a></li>
          <li><a href="/WAI/PF/">Protocols and Formats Working Group</a></li>
          <li><a href="/2008/webapps/">Web Applications Working Group</a></li>
          <li><a href="/2010/webperf/">Web Performance Working Group</a></li>
          <li><a href="/Graphics/SVG/">SVG Working Group</a></li>
        </ul>
        <div>
          <h3 id="external-groups">External groups</h3>
          <ul>
            <li><a href="http://www.atia.org/aia/">Accessibility Interoperability Alliance</a></li>
            <li><a href="http://www.ecma-international.org/memento/TC39.htm">ECMA Technical Committee 39 (TC39)</a></li>
            <li><a href="http://www.openajax.org/">Open Ajax Alliance</a></li>
          </ul>
        </div>
        <div>
          <h3 id="other-technologies">Existing technologies of possible relevance</h3>
          <ul>
            <li><a href="http://msdn.microsoft.com/en-us/library/ms747327.aspx">Microsoft UI Automation</a></li>
            <li><a href="http://developer.apple.com/library/ios/#documentation/DeveloperTools/Reference/UIAutomationRef/_index.html">Apple UI Automation</a></li>
          </ul>
        </div>
      </div>
      <div class="participation">
        <h2 id="participation">
          Participation
        </h2>
        <p>
          To be successful, the Browser Testing and Tools Working Group is
          expected to have ten or more active participants for its
          duration. The Chairs and specification Editors are expected to
          contribute one day per week toward the group. There is no minimum
          requirement for other participants.
        </p>
        <p>
          The Browser Testing and Tools Working Group will also allocate
          the necessary resources for building Test Suites for each of its
          specifications.
        </p>
        <p>
          The group encourages questions and comments on its public mailing
          lists, as described in <a href='#communication'>Communication</a>.
        </p>
        <p>
          The group also welcomes non-Members to contribute technical
          submissions for consideration, with the agreement from each
          participant to Royalty-Free licensing of those submissions under
          the W3C Patent Policy.
        </p>
      </div>
      <div class="communication">
        <h2 id="communication">
          Communication
        </h2>
        <p>
          Any Browser Testing and Tools Working Group teleconferences
          deemed needed will focus on discussion of particular
          specifications, and will be conducted on an as-needed basis, up
          to once per week.
        </p>
        <p>
          The Browser Testing and Tools Working Group primarily conducts
          its work on the public mailing list
          <a href="mailto:public-browser-tools-testing@w3.org">public-browser-tools-testing@w3.org</a>
          (<a
          href="http://lists.w3.org/Archives/Public/public-browser-tools-testing/">archive</a>),
          and on the #testing IRC channel on irc.w3.org:6665.
          The public is invited to post messages to that mailing list and
          to participate in discussions on that IRC channel.
        </p>
        <p>
          Information about the group (deliverables, participants,
          teleconferences, etc.) is available from the
          <a href="/testing/browser/">Browser Testing and Tools Working Group home page</a>.
        </p>
      </div>
      <div class="decisions">
        <h2 id="decisions">
          Decision Policy
        </h2>
        <p>
          In accordance with the W3C Process Document
          (<a href="/Consortium/Process/policies#Consensus">section 3.3</a>),
          the Browser Testing and Tools Working Group will seek to make
          decisions when there is consensus and with due process. The
          expectation is that typically, an editor or other participant
          makes an initial proposal, which is then refined in discussion
          with members of the group and other reviewers, and consensus
          emerges with little formal voting being required. However, if a
          decision is necessary for timely progress, but consensus is not
          achieved after careful consideration of the range of views
          presented, the Chairs should put a question out for voting within
          the group (allowing for remote asynchronous participation—using,
          for example, email and/or web-based survey techniques) and record
          a decision, along with any objections. The matter should then be
          considered resolved unless and until new information becomes
          available.
        </p>
        <p>
          Any decisions or resolutions made by the Browser Testing and
          Tools Working Group will be made in a way that allows for remote,
          asynchronous participation—using, for example, e-mail and/or
          Web-based survey techniques.
        </p>
        <p>
          This charter is written in accordance with
          <a href="/Consortium/Process/policies#Votes">Section 3.4, Votes</a>
          of the W3C Process Document and includes no voting procedures
          beyond what the Process Document requires.
        </p>
      </div>
      <div class="patent">
        <h2 id="patentpolicy">Patent Policy</h2>
        <p>
          The Browser Testing and Tools Working Group operates under the
          <a href="/Consortium/Patent-Policy-20040205/">W3C Patent Policy</a>
          (5 February 2004 Version). To promote the widest adoption of Web
          standards, W3C seeks to issue Recommendations that can be
          implemented, according to this policy, on a Royalty-Free basis.
        </p>
        <p>
          For more information about disclosure obligations for W3C working
          groups, please see the
          <a href="/2004/01/pp-impl/">W3C Patent Policy Implementation</a>.
        </p>
      </div>
      <h2 id="about">
        About this Charter
      </h2>
      <p>
        This charter has been created according to
        <a href="/Consortium/Process/groups#GAGeneral">section 6.2</a>
        of the 
        <a href="/Consortium/Process">W3C Process Document</a>.
        In the event of a conflict between this document or the provisions
        of any charter and the W3C Process, the W3C Process shall take
        precedence.
      </p>
<!--
      <div class="draft">
      <h2>draft notes</h2>
      <ul>
      <li><a href="http://code.google.com/p/selenium/wiki/AdvancedUserInteractions">http://code.google.com/p/selenium/wiki/AdvancedUserInteractions</a></li>
      <li><a href="http://www.chromium.org/developers/testing/webdriver-for-chrome">http://www.chromium.org/developers/testing/webdriver-for-chrome/</a></li>
      <li><a href="http://www.opera.com/developer/tools/operadriver/">http://www.opera.com/developer/tools/operadriver/</a></li>
      <li><a href="http://msdn.microsoft.com/en-us/library/ms747327.aspx">http://msdn.microsoft.com/en-us/library/ms747327.aspx</a></li>
      <li><a href="http://developer.apple.com/library/ios/#documentation/DeveloperTools/Reference/UIAutomationRef/">http://developer.apple.com/library/ios/#documentation/DeveloperTools/Reference/UIAutomationRef/</a></li>
      </ul>
      <ul>
      <li><a href="http://getfirebug.com/logging">http://getfirebug.com/logging</a></li>
      <li><a href="http://getfirebug.com/wiki/index.php/Console_API">http://getfirebug.com/wiki/index.php/Console_API</a></li>
      <li><a href="http://getfirebug.com/wiki/index.php/Command_Line_API">http://getfirebug.com/wiki/index.php/Command_Line_API</a></li>
      <li><a href="http://msdn.microsoft.com/library/dd565625(VS.85).aspx#consolelogging">http://msdn.microsoft.com/library/dd565625(VS.85).aspx#consolelogging</a></li>
      <li><a href="http://getfirebug.com/wiki/index.php/Crossfire">http://getfirebug.com/wiki/index.php/Crossfire</a></li>
      <li><a href="http://code.google.com/p/chromedevtools/wiki/ChromeDevToolsProtocol">http://code.google.com/p/chromedevtools/wiki/ChromeDevToolsProtocol</a></li>
      <li><a href="http://code.google.com/p/v8/wiki/DebuggerProtocol">http://code.google.com/p/v8/wiki/DebuggerProtocol</a></li>
      </ul>
      </div>
-->
      <hr />
      <address>
        Michael[tm] Smith &lt;<a href="mailto:mike@w3.org">mike@w3.org</a>>
      </address>
      <p class="copyright">
        <a rel="Copyright" href="/Consortium/Legal/ipr-notice#Copyright">Copyright</a>© 2011 <a href="http://www.w3.org/"><abbr title="World Wide Web Consortium">W3C</abbr></a> <sup>®</sup> (<a href="http://www.csail.mit.edu/"><abbr title="Massachusetts Institute of Technology">MIT</abbr></a> , <a href="http://www.ercim.eu/"><abbr title="European Research Consortium for Informatics and Mathematics">ERCIM</abbr></a> , <a href="http://www.keio.ac.jp/">Keio</a>), All Rights Reserved.
      </p>
      <p>
        $Id: browser-testing-charter.html,v 1.30 2011/10/07 21:33:24 plehegar Exp $
      </p>
    </div>
  </body>
</html>

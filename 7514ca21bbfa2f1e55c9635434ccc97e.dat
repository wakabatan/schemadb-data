<?xml version="1.0" encoding="utf-8"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:dcterms="http://purl.org/dc/terms/"
xml:base="http://bungeisen.main.jp/metaprof"
xml:lang="en">

<rdf:Description rdf:about="http://bungeisen.main.jp/metaprof">
<rdfs:label>Bungeisen metaprofile</rdfs:label>
<rdfs:comment>This document defines meta element's name attribute values, link element's link types and general element's class attribute values. These use for HTML or XHTML Document. If you use defined identifier in this document for profile attribute value, you can extract metadata by GRDDL.</rdfs:comment>
<dc:creator>hiro</dc:creator>
<dcterms:created>2006-12-16T14:59:41+09:00</dcterms:created>
<dcterms:modified>2008-02-10T23:41:00+09:00</dcterms:modified>
<dc:identifier>http://bungeisen.main.jp/metaprof</dc:identifier>
<dc:relation rdf:resource="http://bungeisen.main.jp/xsl/grddl.xsl" dc:title="XSLT for GRDDL"/>
<dcterms:comformsTo rdf:resource="http://www.w3.org/TR/html401/struct/global.html#profiles"/>
<dcterms:comformsTo rdf:resource="http://www.w3.org/TR/grddl/"/>
<dc:coverage>Bungeisen metaprofile vocabularies use for HTML and XHTML document.</dc:coverage>
</rdf:Description>

<rdf:Property rdf:ID="author">
<rdfs:label>author</rdfs:label>
<rdfs:comment>This is the name of the person who wrote current document. This value can uses for meta element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/creator"/>
</rdf:Property>
<rdf:Property rdf:ID="created">
<rdfs:label>created</rdfs:label>
<rdfs:comment>The current document created this date. This value uses for meta element.The attribute value type must use W3CDTF.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://purl.org/dc/terms/W3CDTF"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/terms/created"/>
</rdf:Property>
<rdf:Property rdf:ID="date">
<rdfs:label>date</rdfs:label>
<rdfs:comment>The current document modified this date. This value uses for meta element.The attribute value type must use W3CDTF.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://purl.org/dc/terms/W3CDTF"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/terms/modified"/>
</rdf:Property>
<rdf:Property rdf:ID="description">
<rdfs:label>description</rdfs:label>
<rdfs:comment>The sentence describes about the current document. This value uses for meta element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/description"/>
</rdf:Property>
<rdf:Property rdf:ID="keywords">
<rdfs:label>keywords</rdfs:label>
<rdfs:comment>The current document contains these subjects of words. This value uses for meta element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/subject"/>
</rdf:Property>
<rdf:Property rdf:ID="logo">
<rdfs:label>logo</rdfs:label>
<rdfs:comment>Refers to an image file which is used for logo or bannar of the current document. This value uses for link element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://purl.org/dc/dcmitype/Image"/>
<rdfs:subPropertyOf rdf:resource="http://xmlns.com/foaf/spec/#term_logo"/>
</rdf:Property>
<rdf:Property rdf:ID="made">
<rdfs:label>made</rdfs:label>
<rdfs:comment>Refers to the author's email address with a mailto URI. This value uses for link element with rev attribute.</rdfs:comment>
<rdfs:domain rdf:resource="http://www.w3.org/2002/07/owl#Thing"/>
<rdfs:range rdf:resource="http://purl.org/dc/terms/URI"/>
<rdfs:isDefinedBy rdf:resource="http://www.w3.org/TR/REC-html32#link"/>
<rdfs:seeAlso rdf:resource="http://www.ietf.org/rfc/rfc2368"/>
</rdf:Property>
<rdf:Property rdf:ID="meta">
<rdfs:label>meta</rdfs:label>
<rdfs:comment>Refers to a metadata document which is related to the current document. This value uses for link element. If this value use with id="foaf", Refers to a FOAF document about the author.</rdfs:comment>
<rdfs:domain rdf:resource="http://www.w3.org/2002/07/owl#Thing"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Resource"/>
<rdfs:isDefinedBy rdf:resource="http://www.w3.org/2000/08/w3c-synd/#meta"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/relation"/>
<rdfs:subPropertyOf rdf:resource="http://xmlns.com/foaf/0.1/isPrimaryTopicOf"/>
</rdf:Property>
<rdf:Property rdf:ID="relation">
<rdfs:label>relation</rdfs:label>
<rdfs:comment>Refers to the resource which is related to the contents of the current document. This value uses for link element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Resource"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/relation"/>
</rdf:Property>
<rdf:Property rdf:ID="source">
<rdfs:label>source</rdfs:label>
<rdfs:comment>Refers to the source document of the current document's content. This value uses for link element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Resource"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/source"/>
</rdf:Property>
<rdf:Property rdf:ID="shortcut">
<rdfs:label>shortcut</rdfs:label>
<rdfs:comment>Refers to an icon(image/vnd.microsoft.ico) file. This value uses for link element. This is used with the link type "icon" for a favicon.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
<rdfs:isDefinedBy rdf:resource="http://support.microsoft.com/kb/415022/"/>
<rdfs:seeAlso rdf:resource="http://www.iana.org/assignments/media-types/image/vnd.microsoft.icon"/>
</rdf:Property>
<rdf:Property rdf:ID="icon">
<rdfs:label>icon</rdfs:label>
<rdfs:comment>Refers to an static image file which is used for favicon of the current document. This value uses for link element.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
<rdfs:isDefinedBy rdf:resource="http://www.w3.org/2005/10/howto-favicon"/>
</rdf:Property>
<rdf:Property rdf:ID="link">
<rdfs:label>link</rdfs:label>
<rdfs:comment>If this value use for paragraph element or unordered list element, it shows related document.</rdfs:comment>
<rdfs:domain rdf:resource="http://xmlns.com/wordnet/1.6/Document"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Resource"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/relation"/>
</rdf:Property>

<rdfs:Class rdf:ID="book">
<rdfs:label>book</rdfs:label>
<rdfs:comment>This value can show about books. This value is used with id attribute which use isbn value "isbn-xxxxxxxxxxxxx". This is "Book Class".</rdfs:comment>
<rdfs:subClassOf rdf:resource="http://www.w3.org/2000/01/rdf-schema#"/>
</rdfs:Class>
<rdf:Property rdf:ID="title">
<rdfs:label>title</rdfs:label>
<rdfs:comment>This value can show the title of book.</rdfs:comment>
<rdfs:domain rdf:resource="http://bungeisen.main.jp/metaprof#book"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/title"/>
</rdf:Property>
<rdf:Property rdf:ID="creator">
<rdfs:label>creator</rdfs:label>
<rdfs:comment>This value can show writer's name.</rdfs:comment>
<rdfs:domain rdf:resource="http://bungeisen.main.jp/metaprof#book"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/creator"/>
</rdf:Property>
<rdf:Property rdf:ID="publisher">
<rdfs:label>publisher</rdfs:label>
<rdfs:comment>This value can show pubrishing company or organization.</rdfs:comment>
<rdfs:domain rdf:resource="http://bungeisen.main.jp/metaprof#book"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/publisher"/>
</rdf:Property>
<rdf:Property rdf:ID="issued">
<rdfs:label>issued</rdfs:label>
<rdfs:comment>This value can show published date of the book.</rdfs:comment>
<rdfs:domain rdf:resource="http://bungeisen.main.jp/metaprof#book"/>
<rdfs:range rdf:resource="http://purl.org/dc/terms/W3CDTF"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/terms/issued"/>
</rdf:Property>
<rdf:Property rdf:ID="review">
<rdfs:label>review</rdfs:label>
<rdfs:comment>This value can show the book review and comment.</rdfs:comment>
<rdfs:domain rdf:resource="http://bungeisen.main.jp/metaprof#book"/>
<rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
<rdfs:subPropertyOf rdf:resource="http://purl.org/dc/elements/1.1/description"/>
</rdf:Property>
</rdf:RDF>
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="http://www.w3.org/StyleSheets/TR/base.css" type="text/css"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
          "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:rss="http://purl.org/rss/1.0/"
 xmlns:dc="http://purl.org/dc/elements/1.1/" xml:lang="en">
<head>
  <title>XHTML Document Development Area</title>
  <style type="text/css">
	ul.toc { list-style: none }
    .tocline { list-style: none; }</style>
  <link rel="stylesheet" type="text/css"
  href="http://www.w3.org/StyleSheets/TR/base.css" />
  <link rel="alternate" type="application/rss+xml"
  title="W3C XHTML Working Group Draft Documents" href="documents.rss" />
</head>

<body>

<div class="head" instanceof="rss:channel">
<a href="http://www.w3.org/"><img height="48" width="72" alt="W3C"
src="http://www.w3.org/Icons/w3c_home" /></a> 

<h1><a id="title">XHTML Document Development Area</a></h1>

<h2>An automatically maintained index of XHTML Working Group documents and
their status</h2>

<h2>Last Update: <!-- automatically maintained - leave on separate line -->
<span id='lastUpdate' property='dc:date' content='2007-11-19T17:33:57+0000'>19 November 2007 17:33</span>
</h2>
</div>

<p>The XHTML Working Group is developing a number of different documents.
Editing of these documents goes on almost constantly. Rather than attempt to
manage formal publication of interim "Editor's Drafts", the working group has
elected to automatically update its document development space each time an
update is made. For information on how this is accomplished, see <a
href="autoPublication.html">the separate document on automatic
publication</a>.</p>

<h1>Table of Contents</h1>
<ul class="toc">
  <li class="tocline"><a href="#curie">CURIEs</a></li>
  <li class="tocline"><a href="#rdfa-syntax">RDFa-Syntax</a></li>
  <li class="tocline"><a href="#xframes">XFrames</a></li>
  <li class="tocline"><a href="#xhtml11">XHTML 1.1</a></li>
  <li class="tocline"><a href="#xhtml2">XHTML 2</a></li>
  <li class="tocline"><a href="#xhtml-access">XHTML Access Module</a></li>
  <li class="tocline"><a href="#xhtml-basic11">XHTML Basic 1.1</a></li>
  <li class="tocline"><a href="#xhtml-modularization">XHTML Modularization
    1.1</a></li>
  <li class="tocline"><a href="#xhtml-role">XHTML Role Attribute
  Module</a></li>
  <li class="tocline"><a href="#xml-events2">XML Events 2.0</a></li>
</ul>

<h1>Active Documents</h1>

<div id="curie" instanceof="rss:item">
<h2>CURIE Syntax 1.0</h2>

<p>A syntax for expressing Compact URIs</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a
      href="http://www.w3.org/TR/2007/WD-curie-20070307">http://www.w3.org/TR/2007/WD-curie-20070307</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-09-05T17:49:31+0000'><a id='curie-ED' href='../2007/ED-curie-20070905'>05 September 2007</a></dd>
  <dt>Editors:</dt>
	<dd>Mark Birbeck, x-port.net Ltd. <a
href="mailto:Mark.Birbeck@x-port.net">&lt;Mark.Birbeck@x-port.net&gt;</a></dd>
    <dd><a href="mailto:shane@aptest.com">Shane McCarron</a>,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a><br />
    </dd>
</dl>
</div>

<div instanceof="rss:item" id="rdfa-syntax">
<h2>RDFa in XHTML: Syntax and Processing</h2>

<p>A description of the syntax and parsing rules for RDFa in XHTML
1.1</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd id="rdfa-syntax-REC">None</dd>
  <dt>Last Public Draft:</dt>
<dd property='dc:date' content='2007-10-18T18:33:26+0000'><a id='rdfa-syntax-WD' href='../2007/WD-rdfa-syntax-20071018'>18 October 2007</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-10-18T13:40:12+0000'><a id='rdfa-syntax-ED' href='../2007/ED-rdfa-syntax-20071018'>18 October 2007</a></dd>
  <dt>Editors:</dt>
<dd>Ben Adida, Creative Commons <a href="mailto:ben@adida.net">
    ben@adida.net</a></dd>
<dd>
  Mark Birbeck,
  <a href="http://www.formsPlayer.com/">x-port.net Ltd.</a>
  <a
href="mailto:mark.birbeck@x-port.net">mark.birbeck@x-port.net</a></dd>


    <dd>Shane McCarron,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a>
			<a href="mailto:shane@aptest.com">shane@aptest.com</a>
    </dd>
<dd>Steven Pemberton, CWI</dd>
</dl>
</div>


<div id="xframes" instanceof="rss:item">
<h2>XFrames</h2>

<p>An XML application for composing documents together</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2005/WD-xframes-20051012">12 October
      2005</a></dd>
  <dt>Last Editor's Draft:</dt>
	<dd>None</dd>
  <dt>Editors:</dt>
    <dd>Steven Pemberton, <a href="http://www.cwi.nl/">CWI</a>/W3C</dd>
    <dd>Masayasu Ishikawa, W3C</dd>
</dl>
</div>

<div id="xhtml11" instanceof="rss:item">
<h2>XHTML 1.1 - module-based XHTML</h2>

<p>An implementation of XHTML 1.0 Strict using XHTML Modularization.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2007/WD-xhtml11-20070216">16 February
      2007</a></dd>
  <dt>Last Editor's Draft:</dt>
    <dd content="2007-04-16T14:15:45+0000"><a id="xhtml11-ED"
      href="../2007/ED-xhtml11-20070416">16 April 2007</a></dd>
<dt>Editors:</dt>

<dd>
<a href="mailto:shane@aptest.com">Shane McCarron</a>, 
<a href="http://www.aptest.com/">Applied Testing
and Technology, Inc.</a><br />
<a href="http://www.w3.org/People/mimasa/">Masayasu Ishikawa</a>, W3C
</dd>


</dl>
</div>

<div id="xhtml2" instanceof="rss:item">
<h2>XHTML 2</h2>

<p>A general-purpose semantic markup language designed with extension in
mind.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2006/WD-xhtml2-20060726">26 July
      2006</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-10-24T16:07:44+0000'><a id='xhtml2-ED' href='../2007/ED-xhtml2-20071024'>24 October 2007</a></dd>
<dt>Editors:</dt>

<dd>Jonny Axelsson, Opera Software</dd>
<dd>Mark Birbeck, x-port.net</dd>
<dd>Micah Dubinko, Invited Expert</dd>
<dd>Beth Epperson, Websense</dd>
<dd><a href="http://www.w3.org/People/mimasa/">Masayasu Ishikawa</a>,
<abbr title="World Wide Web Consortium">W3C</abbr></dd>
<dd><a href="mailto:shane@aptest.com">Shane McCarron</a>, <a
href="http://www.aptest.com">Applied Testing and Technology</a></dd>
<dd>Ann Navarro, Invited Expert</dd>
<dd><a href="http://www.cwi.nl/~steven/">Steven Pemberton</a>, <abbr
title="Centrum voor Wiskunde en Informatica" xml:lang="nl">CWI</abbr>
(<abbr
title="HyperText Markup Language">HTML</abbr> Working Group
Chair)</dd>
</dl>

</dl>
</div>

<div instanceof="rss:item" id="xhtml-access">
<h2>XHTML Access Module</h2>

<p>XHTML Modularization-compatible module for enabling abstract
accessibility in XHTML family documents.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd>None</dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-11-19T17:33:57+0000'><a id='xhtml-access-ED' href='../2007/ED-xhtml-access-20071119'>19 November 2007</a></dd>
 <dt>Editors:</dt>
	<dd><a href="mailto:Mark.Birbeck@x-port.net">Mark Birbeck</a>, <a
	href="http://www.x-port.net">x-port.net Ltd.</a></dd>
    <dd><a href="mailto:shane@aptest.com">Shane McCarron</a>,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a>
    </dd>
     <dd><a href="http://www.cwi.nl/~steven/">Steven Pemberton</a>,
      <a href="http://www.cwi.nl"><abbr lang="nl" xml:lang="nl"
	  title="Centrum voor Wiskunde en Informatica">CWI</abbr></a>/<a
	  href="http://www.w3.org/"><abbr title="World Wide Web
	  Consortium">W3C</abbr></a><sup>&#xae;</sup></dd>
	  <dd><a href="mailto:raman@google.com">T. V. Raman</a>, Google,
Inc.</dd>
	  <dd><a href="mailto:schwer@us.ibm.com">Richard
Schwerdtfeger</a>, IBM Corporation</dd>
</dl>
</div>

<div instanceof="rss:item" id="xhtml-basic11">
<h2>XHTML Basic 1.1</h2>

<p>An XHTML Modularization-based markup language designed for use on smaller
Web clients.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2006/WD-xhtml-basic-20060705">5 July
      2006</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-07-11T14:09:37+0000'><a id='xhtml-basic-ED' href='../2007/ED-xhtml-basic-20070711'>11 July 2007</a></dd>
<dt>Editors:</dt>

<dd>
<a href="mailto:shane@aptest.com">Shane McCarron</a>, Applied Testing
and Technology, Inc.<br />
<a href="mailto:mimasa@w3.org">Masayasu Ishikawa</a>, W3C
</dd>
</dl>
</div>

<div instanceof="rss:item" id="xhtml-modularization">
<h2>XHTML Modularization 1.1</h2>

<p>Modular markup language components and a framework for assembling them
into arbitrary grammars.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a
      href="http://www.w3.org/TR/2006/WD-xhtml-modularization-20060705">5
      July 2006</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-11-15T17:13:25+0000'><a id='xhtml-modularization-ED' href='../2007/ED-xhtml-modularization-20071115'>15 November 2007</a></dd>
<dt>Editors:</dt>

<dd>
<a href="mailto:daniel.b.austin@sun.com">Daniel Austin</a>,
Sun Microsystems<br />
Subramanian Peruvemba, Oracle Corporation<br />
<a href="mailto:shane@aptest.com">Shane McCarron</a>, Applied Testing
and Technology, Inc.<br />
<a href="http://www.w3.org/People/mimasa/">Masayasu Ishikawa</a>, W3C
</dd>
<dd>Mark Birbeck, x-port.net</dd>

</dl>
</div>

<div instanceof="rss:item" id="xhtml-role">
<h2>XHTML Role Attribute Module</h2>

<p>XHTML Modularization-compatible module for adding Role annotation to XHTML
Family markup languages.</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2007/WD-xhtml-role-20071004">4
	October 2007</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-09-05T17:48:07+0000'><a id='xhtml-role-ED' href='../2007/ED-xhtml-role-20070905'>05 September 2007</a></dd>
  <dt>Editors:</dt>
	<dd><a href="mailto:Mark.Birbeck@x-port.net">Mark Birbeck</a>, <a
	href="http://www.x-port.net">x-port.net Ltd.</a></dd>
    <dd><a href="mailto:shane@aptest.com">Shane McCarron</a>,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a>
    </dd>
     <dd><a href="http://www.cwi.nl/~steven/">Steven Pemberton</a>,
      <a href="http://www.cwi.nl"><abbr lang="nl" xml:lang="nl"
	  title="Centrum voor Wiskunde en Informatica">CWI</abbr></a>/<a
	  href="http://www.w3.org/"><abbr title="World Wide Web
	  Consortium">W3C</abbr></a><sup>&#xae;</sup></dd>
	  <dd><a href="mailto:raman@google.com">T. V. Raman</a>, Google,
Inc.</dd>
	  <dd><a href="mailto:schwer@us.ibm.com">Richard
Schwerdtfeger</a>, IBM Corporation</dd>
</dl>
</div>

<div instanceof="rss:item" id="xml-events2">
<h2>XML Events 2</h2>

<p>An events syntax for XML</p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd><a href="http://www.w3.org/TR/2007/WD-xml-events-20070216">16
      February 2007</a></dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-11-14T17:06:05+0000'><a id='xml-events-ED' href='../2007/ED-xml-events-20071114'>14 November 2007</a></dd>
  <dt>Editors:</dt>
    <dd><a href="mailto:shane@aptest.com">Shane McCarron</a>,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a><br />
        <a href="mailto:mark.birbeck@x-port.net">Mark Birbeck</a>,
        <a href="http://www.x-port.net">x-port.net Ltd.</a>
    </dd>
</dl>
</div>

<h1>Documents in Maintenance Mode</h1>

<h2 id="xmlevents1">XML Events 1</h2>

<h2 id="m12ntut">XHTML Modularization Tutorial</h2>

<h2 id="print">XHTML Print</h2>

<h1>Completed Documents</h1>

<h2 id="xhtml1">XHTML 1.0</h2>

<h2 id="m12n1">XHTML Modularization 1.0</h2>

<h1>Abandoned Documents</h1>

<h2 id="hlink">HLink</h2>

<h2 id="m12nschema">XHTML Schema Modularization</h2>

<h2>XHTML RDFa Modules</h2>

<p>XHTML Modularization-compatible modules for adding easy annotation
capabilities to XHTML Family markup languages.
<em>This document was merged with <a href="#rdfa-syntax">RDFa in
XHTML: Syntax and Processing</a>.</em></p>
<dl>
  <dt>Last Recommendation:</dt>
    <dd>None</dd>
  <dt>Last Public Draft:</dt>
    <dd>None</dd>
  <dt>Last Editor's Draft:</dt>
<dd property='dc:date' content='2007-08-11T18:52:47+0000'><a id='xhtml-rdfa-ED' href='../2007/ED-xhtml-rdfa-20070811'>11 August 2007</a></dd>
  <dt>Editors:</dt>
	<dd><a href="mailto:Mark.Birbeck@x-port.net">Mark Birbeck</a>, <a
	href="http://www.x-port.net">x-port.net Ltd.</a></dd>
    <dd><a href="mailto:shane@aptest.com">Shane McCarron</a>,
        <a href="http://www.aptest.com">Applied Testing and
Technology,
            <abbr title="Incorporated">Inc.</abbr></a>
    </dd>
</dl>

<!-- $Revision: 1.76 $
     $Date: 2007/11/19 17:37:40 $
	 $Author: jigsaw $
-->
</body>
</html>

<!-- ...................................................................... -->
<!-- XHTML 1.1 Table Module  .............................................. -->
<!-- file: xhtml11-table-1.mod

     This is XHTML 1.1, a modular variant of XHTML 1.0.
     Copyright 1998-1999 W3C (MIT, INRIA, Keio), All Rights Reserved.
     Revision: @(#)xhtml11-table-1.mod 1.6 99/08/26 SMI

     This DTD module is identified by the PUBLIC and SYSTEM identifiers:

     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Tables 1.0//EN"
     SYSTEM "xhtml11-table-1.mod"

     Revisions:
     (none)
     ....................................................................... -->

<!-- Tables

        table, caption, thead, tfoot, tbody, colgroup, col, tr, th, td

     This module declares element types and attributes used to provide
     table markup similar to HTML 4.0, including features that enable
     better accessibility for non-visual user agents.
-->

<!-- The border attribute sets the thickness of the frame around the
     table. The default units are screen pixels.

     The value "border" is included for backwards compatibility with
     <table border> which yields frame="border" and border="implied".
     For <table border="1"> you get border="1" and frame="implied".
     In this case, it is appropriate to treat this as frame="border"
     for backwards compatibility with deployed browsers.
-->

<!-- The frame attribute specifies which parts of the frame around
     the table should be rendered. The values are not the same as
     CALS to avoid a name clash with the valign attribute.
-->
<!ENTITY % TFrame.attrib
     "frame        ( void
                   | above
                   | below
                   | hsides
                   | lhs
                   | rhs
                   | vsides
                   | box
                   | border )               #IMPLIED"
>

<!-- The rules attribute defines which rules to draw between cells:

     If rules is absent then assume:

       "none" if border is absent or border="0" otherwise "all"
-->
<!ENTITY % TRules.attrib
     "rules        ( none
                   | groups
                   | rows
                   | cols
                   | all )                  #IMPLIED"
>

<!-- horizontal alignment attributes for cell contents
-->
<!ENTITY % CellHAlign.attrib
     "align        ( left
                   | center
                   | right
                   | justify
                   | char )                 #IMPLIED
      char         %Character.datatype;     #IMPLIED
      charoff      %Length.datatype;        #IMPLIED"
>

<!-- vertical alignment attributes for cell contents
-->
<!ENTITY % CellVAlign.attrib
     "valign       ( top
                   | middle
                   | bottom
                   | baseline )             #IMPLIED"
>

<!-- scope is simpler than axes attribute for common tables
-->
<!ENTITY % Scope.attrib
     "scope        ( row
                   | col
                   | rowgroup
                   | colgroup )             #IMPLIED"
>

<!-- table: Table Element .............................. -->

<!ENTITY % Table.element  "INCLUDE" >
<![%Table.element;[
<!ENTITY % Table.content
     "( caption?, ( col* | colgroup* ),
      (( thead?, tfoot?, tbody+ ) | ( tr+ )))"
>
<!ELEMENT table  %Table.content; >
<!-- end of Table.element -->]]>

<!ENTITY % Table.attlist  "INCLUDE" >
<![%Table.attlist;[
<!ATTLIST table
      %Common.attrib;
      summary      %Text.datatype;          #IMPLIED
      width        %Length.datatype;        #IMPLIED
      border       %Pixels.datatype;        #IMPLIED
      %TFrame.attrib;
      %TRules.attrib;
      cellspacing  %Length.datatype;        #IMPLIED
      cellpadding  %Length.datatype;        #IMPLIED
      datapagesize CDATA                    #IMPLIED
>
<!-- end of Table.attlist -->]]>

<!-- caption: Table Caption ............................ -->

<!ENTITY % Caption.element  "INCLUDE" >
<![%Caption.element;[
<!ENTITY % Caption.content
     "( #PCDATA | %Inline.mix; )*"
>
<!ELEMENT caption  %Caption.content; >
<!-- end of Caption.element -->]]>

<!ENTITY % Caption.attlist  "INCLUDE" >
<![%Caption.attlist;[
<!ATTLIST caption
      %Common.attrib;
>
<!-- end of Caption.attlist -->]]>

<!-- thead: Table Header ............................... -->

<!-- Use thead to duplicate headers when breaking table
     across page boundaries, or for static headers when
     tbody sections are rendered in scrolling panel.
-->

<!ENTITY % Thead.element  "INCLUDE" >
<![%Thead.element;[
<!-- end of Thead.element -->]]>
<!ENTITY % Thead.content  "( tr )+" >
<!ELEMENT thead  %Thead.content; >

<!ENTITY % Thead.attlist  "INCLUDE" >
<![%Thead.attlist;[
<!ATTLIST thead
      %Common.attrib;
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Thead.attlist -->]]>

<!-- tfoot: Table Footer ............................... -->

<!-- Use tfoot to duplicate footers when breaking table
     across page boundaries, or for static footers when
     tbody sections are rendered in scrolling panel.
-->

<!ENTITY % Tfoot.element  "INCLUDE" >
<![%Tfoot.element;[
<!ENTITY % Tfoot.content  "( tr )+" >
<!ELEMENT tfoot  %Tfoot.content; >
<!-- end of Tfoot.element -->]]>

<!ENTITY % Tfoot.attlist  "INCLUDE" >
<![%Tfoot.attlist;[
<!ATTLIST tfoot
      %Common.attrib;
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Tfoot.attlist -->]]>

<!-- tbody: Table Body ................................. -->

<!-- Use multiple tbody sections when rules are needed
     between groups of table rows.
-->

<!ENTITY % Tbody.element  "INCLUDE" >
<![%Tbody.element;[
<!ENTITY % Tbody.content  "( tr )+" >
<!ELEMENT tbody  %Tbody.content; >
<!-- end of Tbody.element -->]]>

<!ENTITY % Tbody.attlist  "INCLUDE" >
<![%Tbody.attlist;[
<!ATTLIST tbody
      %Common.attrib;
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Tbody.attlist -->]]>

<!-- colgroup: Table Column Group ...................... -->

<!-- colgroup groups a set of col elements. It allows you
     to group several semantically-related columns together.
-->

<!ENTITY % Colgroup.element  "INCLUDE" >
<![%Colgroup.element;[
<!ENTITY % Colgroup.content  "( col )*" >
<!ELEMENT colgroup  %Colgroup.content; >
<!-- end of Colgroup.element -->]]>

<!ENTITY % Colgroup.attlist  "INCLUDE" >
<![%Colgroup.attlist;[
<!-- end of Colgroup.attlist -->]]>
<!ATTLIST colgroup
      %Common.attrib;
      span         %Number.datatype;        '1'
      width        %MultiLength.datatype;   #IMPLIED
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>

<!-- col: Table Column ................................. -->

<!-- col elements define the alignment properties for
     cells in one or more columns.

     The width attribute specifies the width of the
     columns, e.g.

       width="64"        width in screen pixels
       width="0.5*"      relative width of 0.5

     The span attribute causes the attributes of one
     col element to apply to more than one column.
-->

<!ENTITY % Col.element  "INCLUDE" >
<![%Col.element;[
<!ENTITY % Col.content  "EMPTY" >
<!ELEMENT col  %Col.content; >
<!-- end of Col.element -->]]>

<!ENTITY % Col.attlist  "INCLUDE" >
<![%Col.attlist;[
<!ATTLIST col
      %Common.attrib;
      span         %Number.datatype;        '1'
      width        %MultiLength.datatype;   #IMPLIED
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Col.attlist -->]]>

<!-- tr: Table Row ..................................... -->

<!ENTITY % Tr.element  "INCLUDE" >
<![%Tr.element;[
<!ENTITY % Tr.content  "( th | td )+" >
<!ELEMENT tr  %Tr.content; >
<!-- end of Tr.element -->]]>

<!ENTITY % Tr.attlist  "INCLUDE" >
<![%Tr.attlist;[
<!ATTLIST tr
      %Common.attrib;
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Tr.attlist -->]]>

<!-- th: Table Header Cell ............................. -->

<!-- th is for header cells, td for data,
     but for cells acting as both use td
-->

<!ENTITY % Th.element  "INCLUDE" >
<![%Th.element;[
<!ENTITY % Th.content
     "( #PCDATA | %Flow.mix; )*"
>
<!ELEMENT th  %Th.content; >
<!-- end of Th.element -->]]>

<!ENTITY % Th.attlist  "INCLUDE" >
<![%Th.attlist;[
<!ATTLIST th
      %Common.attrib;
      abbr         %Text.datatype;          #IMPLIED
      axis         CDATA                    #IMPLIED
      headers      IDREFS                   #IMPLIED
      %Scope.attrib;
      rowspan      %Number.datatype;        '1'
      colspan      %Number.datatype;        '1'
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Th.attlist -->]]>

<!-- td: Table Data Cell ............................... -->

<!ENTITY % Td.element  "INCLUDE" >
<![%Td.element;[
<!ENTITY % Td.content
     "( #PCDATA | %Flow.mix; )*"
>
<!ELEMENT td  %Td.content; >
<!-- end of Td.element -->]]>

<!ENTITY % Td.attlist  "INCLUDE" >
<![%Td.attlist;[
<!ATTLIST td
      %Common.attrib;
      abbr         %Text.datatype;          #IMPLIED
      axis         CDATA                    #IMPLIED
      headers      IDREFS                   #IMPLIED
      %Scope.attrib;
      rowspan      %Number.datatype;        '1'
      colspan      %Number.datatype;        '1'
      %CellHAlign.attrib;
      %CellVAlign.attrib;
>
<!-- end of Td.attlist -->]]>

<!-- end of xhtml11-table-1.mod -->

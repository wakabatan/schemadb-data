<!-- ....................................................................... -->
<!-- XHTML 1.1 DTD  ........................................................ -->
<!-- file: xhtml11.dtd
-->

<!-- XHTML 1.1 DTD

     This is XHTML 1.1, a modular variant of XHTML 1.0.

     Copyright 1998-1999 World Wide Web Consortium
        (Massachusetts Institute of Technology, Institut National de
         Recherche en Informatique et en Automatique, Keio University).
         All Rights Reserved.

     Permission to use, copy, modify and distribute the XHTML 1.1 DTD and
     its accompanying documentation for any purpose and without fee is
     hereby granted in perpetuity, provided that the above copyright notice
     and this paragraph appear in all copies.  The copyright holders make
     no representation about the suitability of the DTD for any purpose.

     It is provided "as is" without expressed or implied warranty.

        Author:     Murray M. Altheim <altheim@eng.sun.com>
        Revision:   @(#)xhtml11.dtd 1.10 99/08/26 SMI

-->
<!-- This is the driver file for version 1.1 of the XHTML DTD.

     Please use this formal public identifier to identify it:

         "-//W3C//DTD XHTML 1.1//EN"

     Please use this URI to identify the default namespace:

         "http://www.w3.org/TR/xhtml1"

     For example, if you are using XHTML 1.1 directly, use the FPI
     in the DOCTYPE declaration, with the xmlns attribute on the
     document element to identify the default namespace:

         <?xml version="1.0"?>
         <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
                               "xhtml11.dtd" >
         <html xmlns="http://www.w3.org/TR/xhtml1"
               xmlns:xlink="http://www.w3.org/XML/XLink/0.9"
               xml:lang="en" >
         ...
         </html>

     Revisions:
     (none)
-->

<!-- The xmlns attribute on <html> identifies the
     default namespace to namespace-aware applications:
-->
<!ENTITY % XHTML.ns  "http://www.w3.org/TR/xhtml1" >

<!-- reserved for future use with document profiles -->
<!ENTITY % XHTML.profile  "" >

<!-- Internationalization features
     This feature-test entity is used to declare elements
     and attributes used for internationalization support.
-->
<!ENTITY % XHTML.I18n            "INCLUDE" >

<!-- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->

<!-- Redeclaration placeholder  .................................. -->
<!ENTITY % xhtml-redecl.module "IGNORE" >
<![%xhtml-redecl.module;[
%xhtml-redecl.mod;]]>

<!ENTITY % xhtml-events.module "INCLUDE" >

<!-- Modular Framework Module  ................................... -->
<!ENTITY % xhtml-framework.mod
     PUBLIC "-//W3C//ENTITIES XHTML 1.1 Modular Framework 1.0//EN"
            "xhtml11-framework-1.mod" >
%xhtml-framework.mod;

<!-- Basic Text Module (Required)  ............................... -->
<!ENTITY % xhtml-text.module "INCLUDE" >
<![%xhtml-text.module;[
<!ENTITY % xhtml-text.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Basic Text 1.0//EN"
            "xhtml11-text-1.mod" >
%xhtml-text.mod;]]>

<!-- Hypertext Module (required) ................................. -->
<!ENTITY % xhtml-hypertext.module "INCLUDE" >
<![%xhtml-hypertext.module;[
<!ENTITY % xhtml-hypertext.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Hypertext 1.0//EN"
            "xhtml11-hypertext-1.mod" >
%xhtml-hypertext.mod;]]>

<!-- Lists Module (required)  .................................... -->
<!ENTITY % xhtml-list.module "INCLUDE" >
<![%xhtml-list.module;[
<!ENTITY % xhtml-list.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Lists 1.0//EN"
            "xhtml11-list-1.mod" >
%xhtml-list.mod;]]>

<!-- Edit Module  ................................................ -->
<!ENTITY % xhtml-edit.module "INCLUDE" >
<![%xhtml-edit.module;[
<!ENTITY % xhtml-edit.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Editing Elements 1.0//EN"
            "xhtml11-edit-1.mod" >
%xhtml-edit.mod;]]>

<!-- Presentation Module  ........................................ -->
<!ENTITY % xhtml-pres.module "INCLUDE" >
<![%xhtml-pres.module;[
<!ENTITY % xhtml-pres.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Presentation 1.0//EN"
            "xhtml11-pres-1.mod" >
%xhtml-pres.mod;]]>

<!-- Java Applet Element Module  ................................. -->
<!ENTITY % xhtml-applet.module "INCLUDE" >
<![%xhtml-applet.module;[
<!ENTITY % xhtml-applet.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Java Applets 1.0//EN"
            "xhtml11-applet-1.mod" >
%xhtml-applet.mod;]]>

<!-- BIDI Override Module  ....................................... -->
<!ENTITY % xhtml-bdo.module "INCLUDE" >
<![%xhtml-bdo.module;[
<!ENTITY % xhtml-bdo.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 BIDI Override Element 1.0//EN"
            "xhtml11-bdo-1.mod" >
%xhtml-bdo.mod;]]>

<!-- Forms Module  ............................................... -->
<!ENTITY % xhtml-form.module "INCLUDE" >
<![%xhtml-form.module;[
<!ENTITY % xhtml-form.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Forms 1.0//EN"
            "xhtml11-form-1.mod" >
%xhtml-form.mod;]]>

<!-- Tables Module ............................................... -->
<!ENTITY % xhtml-table.module "INCLUDE" >
<![%xhtml-table.module;[
<!ENTITY % xhtml-table.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Tables 1.0//EN"
            "xhtml11-table-1.mod" >
%xhtml-table.mod;]]>

<!-- Image Module  ............................................... -->
<!ENTITY % xhtml-image.module "INCLUDE" >
<![%xhtml-image.module;[
<!ENTITY % xhtml-image.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Images 1.0//EN"
            "xhtml11-image-1.mod" >
%xhtml-image.mod;]]>

<!-- Client-side Image Map Module  ............................... -->
<!ENTITY % xhtml-csismap.module "INCLUDE" >
<![%xhtml-csismap.module;[
<!ENTITY % xhtml-csismap.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Client-side Image Maps 1.0//EN"
            "xhtml11-csismap-1.mod" >
%xhtml-csismap.mod;]]>

<!-- Server-side Image Map Module  ............................... -->
<!ENTITY % xhtml-ssismap.module "INCLUDE" >
<![%xhtml-ssismap.module;[
<!ENTITY % xhtml-ssismap.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Server-side Image Maps 1.0//EN"
            "xhtml11-ssismap-1.mod" >
%xhtml-ssismap.mod;]]>

<!-- Document Metainformation Module  ............................ -->
<!ENTITY % xhtml-meta.module "INCLUDE" >
<![%xhtml-meta.module;[
<!ENTITY % xhtml-meta.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Metainformation 1.0//EN"
            "xhtml11-meta-1.mod" >
%xhtml-meta.mod;]]>

<!-- Scripting Module  ........................................... -->
<!ENTITY % xhtml-script.module "INCLUDE" >
<![%xhtml-script.module;[
<!ENTITY % xhtml-script.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Scripting 1.0//EN"
            "xhtml11-script-1.mod" >
%xhtml-script.mod;]]>

<!-- Stylesheets Module  ......................................... -->
<!ENTITY % xhtml-style.module "INCLUDE" >
<![%xhtml-style.module;[
<!ENTITY % xhtml-style.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Stylesheets 1.0//EN"
            "xhtml11-style-1.mod" >
%xhtml-style.mod;]]>

<!-- Link Element Module  ........................................ -->
<!ENTITY % xhtml-link.module "INCLUDE" >
<![%xhtml-link.module;[
<!ENTITY % xhtml-link.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Link Element 1.0//EN"
            "xhtml11-link-1.mod" >
%xhtml-link.mod;]]>

<!-- Base Element Module  ........................................ -->
<!ENTITY % xhtml-base.module "INCLUDE" >
<![%xhtml-base.module;[
<!ENTITY % xhtml-base.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Base Element 1.0//EN"
            "xhtml11-base-1.mod" >
%xhtml-base.mod;]]>

<!-- Document Structure Module (required)  ....................... -->
<!ENTITY % xhtml-struct.module "INCLUDE" >
<![%xhtml-struct.module;[
<!ENTITY % xhtml-struct.mod
     PUBLIC "-//W3C//ELEMENTS XHTML 1.1 Document Structure 1.0//EN"
            "xhtml11-struct-1.mod" >
%xhtml-struct.mod;]]>

<!-- end of XHTML 1.1 DTD  ................................................. -->
<!-- ....................................................................... -->

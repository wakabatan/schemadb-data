<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta name="robots" content="index,nofollow">


<title>Code - Delay Tolerant Networking Research Group</title>

<script type="text/javascript">
<!--// common functions

// We keep here the state of the search box
searchIsDisabled = false;

function searchChange(e) {
    // Update search buttons status according to search box content.
    // Ignore empty or whitespace search term.
    var value = e.value.replace(/\s+/, '');
    if (value == '' || searchIsDisabled) { 
        searchSetDisabled(true);
    } else {
        searchSetDisabled(false);
    }
}

function searchSetDisabled(flag) {
    // Enable or disable search
    document.getElementById('fullsearch').disabled = flag;
    document.getElementById('titlesearch').disabled = flag;
}

function searchFocus(e) {
    // Update search input content on focus
    if (e.value == 'Search') {
        e.value = '';
        e.style.color = 'black';
        searchIsDisabled = false;
    }
}

function searchBlur(e) {
    // Update search input content on blur
    if (e.value == '') {
        e.value = 'Search';
        e.style.color = 'gray';
        searchIsDisabled = true;
    }
}

function actionsMenuInit(title) {
    // Initiliaze action menu
    for (i = 0; i < document.forms.length; i++) {
        var form = document.forms[i];
        if (form.className == 'actionsmenu') {
            // Check if this form needs update
            var div = form.getElementsByTagName('div')[0];
            var label = div.getElementsByTagName('label')[0];
            if (label) {
                // This is the first time: remove label and do buton.
                div.removeChild(label);
                var dobutton = div.getElementsByTagName('input')[0];
                div.removeChild(dobutton);
                // and add menu title
                var select = div.getElementsByTagName('select')[0];
                var item = document.createElement('option');
                item.appendChild(document.createTextNode(title));
                item.value = 'show';
                select.insertBefore(item, select.options[0]);
                select.selectedIndex = 0;
            }
        }
    }
}
//-->
</script>

<link rel="stylesheet" type="text/css" charset="utf-8" media="all" href="/wiki_docs/rightsidebar/css/common.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="screen" href="/wiki_docs/rightsidebar/css/screen.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="print" href="/wiki_docs/rightsidebar/css/print.css">
<link rel="stylesheet" type="text/css" charset="utf-8" media="projection" href="/wiki_docs/rightsidebar/css/projection.css">

<link rel="Start" href="/wiki/Home">
<link rel="Alternate" title="Wiki Markup" href="/wiki/Code?action=raw">
<link rel="Alternate" media="print" title="Print View" href="/wiki/Code?action=print">
<link rel="Search" href="/wiki/FindPage">
<link rel="Index" href="/wiki/TitleIndex">
<link rel="Glossary" href="/wiki/WordIndex">
<link rel="Help" href="/wiki/HelpOnFormatting">
</head>

<body  lang="en" dir="ltr">

<div id="header">

<form id="searchform" method="get" action="">
<div>
<input type="hidden" name="action" value="fullsearch">
<input type="hidden" name="context" value="180">
<label for="searchinput">Search:</label>
<input id="searchinput" type="text" name="value" value="" size="20"
    onfocus="searchFocus(this)" onblur="searchBlur(this)"
    onkeyup="searchChange(this)" onchange="searchChange(this)" alt="Search">
<input id="titlesearch" name="titlesearch" type="submit"
    value="Titles" alt="Search Titles">
<input id="fullsearch" name="fullsearch" type="submit"
    value="Text" alt="Search Full Text">
</div>
</form>
<script type="text/javascript">
<!--// Initialize search form
var f = document.getElementById('searchform');
f.getElementsByTagName('label')[0].style.display = 'none';
var e = document.getElementById('searchinput');
searchChange(e);
searchBlur(e);
//-->
</script>

<div id="logo"><a href="/wiki/Home">Delay Tolerant Networking Research Group</a></div>
</div>

<div id="sidebar">
<div class="sidepanel">
<h1>Wiki</h1>

<ul id="navibar">
<li class="wikilink"><a href="/wiki/Home">Home</a></li>
<li class="wikilink"><a href="/wiki/Docs">Docs</a></li>
<li class="wikilink current"><a href="/wiki/Code">Code</a></li>
<li class="wikilink"><a href="/wiki/About">About</a></li>
<li class="wikilink"><a href="/wiki/RecentChanges">RecentChanges</a></li>
<li class="wikilink"><a href="/wiki/FindPage">FindPage</a></li>
<li class="wikilink"><a href="/wiki/HelpContents">HelpContents</a></li>
</ul>

</div>
<div class="sidepanel">
<h1>Page</h1>
<ul class="editbar">
<li>Immutable Page</li>
<li><a href="/wiki/Code?action=diff">Show Last Change</a></li>
<li><a href="/wiki/Code?action=info">Get Info</a></li>
<li>
<form class="actionsmenu" method="get" action="">
<div>
    <label>More Actions:</label>
    <select name="action"
        onchange="if ((this.selectedIndex != 0) &&
                      (this.options[this.selectedIndex].disabled == false)) {
                this.form.submit();
            }
            this.selectedIndex = 0;">
        <option value="raw">Show Raw Text</option>
<option value="print">Show Print View</option>
<option value="refresh">Delete Cache</option>
<option value="show" disabled class="disabled">--------</option>
<option value="AttachFile">Attachments</option>
<option value="SpellCheck">Check Spelling</option>
<option value="LikePages">Show Like Pages</option>
<option value="LocalSiteMap">Show Local Site Map</option>
<option value="show" disabled class="disabled">--------</option>
<option value="RenamePage" disabled class="disabled">Rename Page</option>
<option value="DeletePage" disabled class="disabled">Delete Page</option>
    </select>
    <input type="submit" value="Do">
</div>
<script type="text/javascript">
<!--// Init menu
actionsMenuInit('More Actions:');
//-->
</script>
</form>
</li>
</ul>

</div>
<div class="sidepanel">
<h1>User</h1>
<ul id="username"><li><a href="/wiki/UserPreferences">Login</a></li></ul>

</div>
<ul id="credits">
<li><a href="http://moinmoin.wikiwikiweb.de/">MoinMoin Powered</a></li>
<li><a href="http://www.python.org/">Python Powered</a></li>
<li><a href="http://validator.w3.org/check?uri=referer"><img
  src="http://www.w3.org/Icons/valid-html401"
  alt="Valid HTML 4.01!" height="20" width="52"></a></li>
</ul>

</div>

<div id="page" lang="en" dir="ltr"><!-- start page -->
<div lang="en" id="content" dir="ltr">
<a id="top"></a>

<h2 id="head-af6944977150c10d42bd9d4c807754898586efc1">Can I get any code?</h2>


<h3 id="head-ad798bf5f38187543f3019c64695e61bf884bf25">The bundle protocol and the Lickider transmission protocol (LTP)</h3>

<p>The DTNRG are currently working on two protocols for which there is code publicly available. The bundle protocol is a general overlay  network protocol, and is the focus of most of the implementation  effort in DTNRG. There is a reference implementation of the bundle protocol which is called DTN2 and is documented below. </p>
<p>The Licklider transmission protocol (LTP - sometimes called the  longhaul transmission protocol) is a point-to-point protocol  intended for over very high delay links such as those used in deep space communications. There are two LTP implementations available -- one from  <a class="external" href="http://irg.cs.ohiou.edu/ocp/ltp.html">Ohio University (in Java)</a> and one from <a class="external" href="https://down.dsg.cs.tcd.ie/ltplib/">Trinity College Dublin, Ireland (in C++)</a>. </p>

<h3 id="head-a76bcc32534a0cfc9682b17834e3285ee4a76b13">DTN2</h3>

<p>The latest release of the DTN reference implementation is version 2.4.0, released in July 2007. </p>
<p>The goal of this implementation is to clearly embody the components of the DTN  architecture, while also providing a robust and flexible software framework for  experimentation, extension, and real-world deployment. An early version of this  implementation was used for the <a class="external" href="http://www.melissaho.com/research/dtn/demo">DTN Train Demo</a> at the Intel Research Berkeley Open House 2004 and the current version was used for the demo  in February 2006. Please report any bugs to dtn-users _at_ mailman.dtnrg.org </p>
<ul>
<li style="list-style-type:none"><p>Source: <a class="external" href="http://www.dtnrg.org/docs/code/dtn_2.4.0.tgz">dtn_2.4.0.tgz</a>, <a class="external" href="http://www.dtnrg.org/docs/code/DTN2/README">README</a>, <a class="external" href="http://www.dtnrg.org/docs/code/DTN2/STATUS">STATUS</a>, <a class="external" href="http://www.dtnrg.org/docs/code/DTN2/RELEASE-NOTES">RELEASE-NOTES</a> </p>
<p><a class="external" href="http://www.dtnrg.org/docs/code/DTN2/doc/manual/">Manual</a> (html, included with source) </p>
<p><a class="external" href="http://www.dtnrg.org/docs/code/DTN2/doc/doxygen/html/index.html">Doxygen generated code documentation</a> (included with source) </p>
<p><a href="/wiki/Dtn2Documentation"> Documentation</a> (wiki) </p>
<p><a href="/wiki/Dtn2TipsAndTricks"> Tips and Tricks for Compiling and Running DTN2</a> </p>
<p><a href="/wiki/Dtn2KnownBugs"> Known Bugs in DTN2</a> </p>
</li>
</ul>
<p>This release has been tested on Linux, Mac OS X, and Windows (with Cygwin). Limited support has been tested on Solaris and Linux on arm. </p>
<p>Before you configure and compile you will need GCC 3.3 or newer, and development packages of TCL 8.3 or 8.4. Optionally, you may also want BerkeleyDB 4.2-4.5 as a back-end database, and xerces 2.6+ for XML processing. </p>

<h4 id="head-5e8b5494a3210d22f14910aa23f1149e9882f690">Anonymous CVS</h4>

<p>You can also access the latest ("bleeding edge") version of the code using the anonymous pserver. Using CVS, you can do the following: </p>
<ul>
<li style="list-style-type:none"><p>% cvs -d :pserver:anonymous@<strong></strong>code.dtnrg.org:/repository login </p>
<p>% cvs -d :pserver:anonymous@<strong></strong>code.dtnrg.org:/repository checkout DTN2 </p>
</li>
</ul>
<p>The password is 'bundles'.  Please be aware this code is under frequent  developmental changes, so your mileage may vary.  In addition, if you grab the  development copy in this way, any README files or other docs may not be quite up to  date.  </p>
<p>Please report any bugs to dtn-users _at_ mailman _dot_ dtnrg _dot_ org </p>

<h4 id="head-01416ee696560c5df8cab5f007717bc40707ec2d">Debian Users</h4>

<p>This release is also available as a binary debian package for the  stable debian release (etch) and old stable release (sarge) on x86,  as well as in source form through the apt package management system.  Add the following lines to /etc/apt/sources.list: </p>
<ul>
<li style="list-style-type:none"><p>deb     <a href="http://www.dtnrg.org/debian">http://www.dtnrg.org/debian</a> etch contrib </p>
<p>deb-src <a href="http://www.dtnrg.org/debian">http://www.dtnrg.org/debian</a> etch contrib </p>
</li>
</ul>
<p>or </p>
<ul>
<li style="list-style-type:none"><p>deb     <a href="http://www.dtnrg.org/debian">http://www.dtnrg.org/debian</a> sarge contrib </p>
<p>deb-src <a href="http://www.dtnrg.org/debian">http://www.dtnrg.org/debian</a> etch contrib </p>
</li>
</ul>
<p>The package is called 'dtn'. </p>

<h4 id="head-5367eb40cbfb8ca2aa2e57522f4b927cddef6e1c">Redhat / Fedora Users</h4>

<p>We have pre-built some rpm binary packages (2.3.0 only): </p>
<ul>
<li style="list-style-type:none"><p>Fedora Core 6:   <a class="external" href="http://www.dtnrg.org/docs/code/fc6-dtn-2.3.0-1.i386.rpm">fc6-dtn-2.3.0-1.i386.rpm</a> </p>
</li>
</ul>

<h4 id="head-529936d7460faaa73636514a05725aa39ad0da35">Simulation</h4>

<p>Waterloo has written the <a class="external" href="http://watwire.uwaterloo.ca/DTN/sim/">DTNSim2 Simulator</a> based on the  <a class="external" href="http://www.dtnrg.org/code/dtnsim.tgz">java-based simulator (gzip/tar)</a> developed by Sushant Jain (who is now at Google). </p>
<p>There is also a simulator using user mode Linux located <a class="external" href="http://dtnvnuml.ehas.org/">here</a>. </p>

<h3 id="head-c02d78a8be5cfd9b599cf60f1761d2c4b769a0d5">Other Implementations</h3>

<p>* <a class="external" href="http://irg.cs.ohiou.edu/ocp/bundling.html">Java BP-RI</a>: A Java implementation of the bundling protocol that is based significantly on the design of DTN2. The BP-RI supports just LTP as a convergence layer, currently, interfacing with the <a class="external" href="http://irg.cs.ohiou.edu/ocp/ltp.html">LTP-RI</a> implementation in Java. </p>
<p>* <a class="external" href="http://www.netlab.tkk.fi/~jo/dtn/index.html">DASM</a>: A DTN implementation for Symbian phones (tested with Nokia Communicators 9300i and 9500).  This is an independent implementation tailored to the particular needs of the Symbian OS. </p>
<p>* DTN1: This is a prototype implementation of the DTN architecture, written in C, and tested on both x86 and StrongARM versions of Linux.  Please read the top-level README.txt file for details on how to build and use it. </p>
<ul>
<li style="list-style-type:none"><p>You can access the code for this version via anonymous CVS: </p>
<ul>
<li style="list-style-type:none"><p>% cvs -d :pserver:anonymous@<strong></strong>code.dtnrg.org:/repository login </p>
<p>% cvs -d :pserver:anonymous@<strong></strong>code.dtnrg.org:/repository checkout DTN1 </p>
</li>
</ul>
<p>The first code release was accomplished as of 30-Mar-2003 and is available here: <a class="external" href="http://www.dtnrg.org/docs/code/dtn-release-0.1.tgz">dtn-release-0.1.tgz</a> </p>
</li>
</ul>
<p>* <a class="external" href="http://sourceforge.net/projects/dtn/">Java DTN</a>: There is also another Java implementation of DTN, currently being developed by Matt Bradbury. This version is fairly old at this point. </p>
<a id="bottom"></a>

</div>
<div id="pagebottom"></div>
<p id="pageinfo" class="info" lang="en" dir="ltr">last edited 2007-07-17 18:25:11 by <span title="bldmz-nat-161-182.berkeley.intel-research.net"><a href="/wiki/demmer">demmer</a></span></p>

</div> <!-- end page -->


</body>
</html>


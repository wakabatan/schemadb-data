CAT	= cat
CVS	= cvs
CVS_ADD = $(CVS) add -kb
CVS_RM  = $(CVS) rm
CVS_COMMIT = $(CVS) commit -m "auto-committed"
CVS_LOGNAME = make
PERL = perl
RM  = rm -fv
XARGS = xargs

#SWBIN = ../../bin
SWBIN = .
DIFFFL = $(PERL) $(SWBIN)/difffl.pl

TMP_FILE_LIST = .files.tmp
TMP_FILE_LIST_ADDED = .files-added.tmp
TMP_FILE_LIST_REMOVED = .files-removed.tmp

VERSIONING_FILE_FILTER = \.(?:txt|dat|db|ns|ref|prop)\Z

cvs-commit: versioning-sync
	LOGNAME=$(CVS_LOGNAME) $(CVS_COMMIT)

versioning-sync: file-list-diff
	-$(CAT) $(TMP_FILE_LIST_ADDED) | $(XARGS) $(CVS_ADD) dummy
	-$(CAT) $(TMP_FILE_LIST_REMOVED) | $(XARGS) $(CVS_RM) dummy

file-list-diff:
	$(DIFFFL) --list-file=$(TMP_FILE_LIST) \
	          --added-list-file=$(TMP_FILE_LIST_ADDED) \
	          --removed-list-file=$(TMP_FILE_LIST_REMOVED) \
	          --target-filter="$(VERSIONING_FILE_FILTER)"

clean:
	$(RM) *.bak *~ *.BAK
	$(RM) .*.tmp .*~
	$(RM) $(TMP_FILE_LIST_ADDED) $(TMP_FILE_LIST_REMOVED)

distclean: clean
	$(RM) $(TMP_FILE_LIST)

## $Date: 2007/10/06 17:17:21 $
## License: Public Domain.

<!-- ...................................................................... -->
<!-- aaa-qname Module ................................................... -->
<!-- file: aaa-qname.mod

  PUBLIC "-//W3C//DTD XHTML 1.1 For Accessible Adaptable Applications Namespace//EN"
SYSTEM "http://www.w3.org/2005/07/aaa/aaa-qname.mod">

xmlns:aaa="http://www.w3.org/2005/07/aaa "
...................................................................... -->

<!ENTITY % XHTML.version
    "-//W3C//DTD XHTML 1.1 For Accessible Adaptable Applications Namespace//EN" >

<!-- 1. Namespace declarations ::::::::::::::::::::::::::::: -->

<!ENTITY % NS.prefixed     "IGNORE" >
<!ENTITY % aaa.prefixed "%NS.prefixed;" >

<!-- Declare the actual namespace of this module -->

<!ENTITY % aaa.xmlns "http://www.w3.org/2005/07/aaa" >

<!ENTITY % aaa.prefix   "aaa" >
<![%aaa.prefixed;[
<!ENTITY % aaa.xmlns.extra.attrib  "" >
]]>

<!ENTITY % aaa.xmlns.extra.attrib
     "" >

<![%aaa.prefixed;[
<!ENTITY % aaa.pfx  "%aaa.prefix;:" >

<!ENTITY % aaa.xmlns.attrib
     "xmlns:%aaa.prefix;  CDATA   #FIXED '%aaa.xmlns;'
      %aaa.xmlns.extra.attrib;"
>
]]>
<!ENTITY % aaa.pfx  "" >
<!ENTITY % aaa.xmlns.attrib
     "xmlns        CDATA           #FIXED '%aaa.xmlns;'
      %aaa.xmlns.extra.attrib;"
>

<![%NS.prefixed;[
<!ENTITY % XHTML.xmlns.extra.attrib
     "%aaa.xmlns.attrib;" >

]]>

<!-- 2. XML Qualified Names for AAA ::::::::::::::::::::::::::::: -->

<!--   This section declares parameter entities used to provide
        namespace-qualified names for all element types.
-->

<!ENTITY % xhtml-datatypes.mod
     PUBLIC "-//W3C//ENTITIES XHTML Datatypes 1.0//EN"
            "http://www.w3.org/TR/xhtml-modularization/DTD/xhtml-datatypes-1.mod" >
%xhtml-datatypes.mod;

<!-- core attributes to add to all elements; -->

<!-- states -->

<!ENTITY % aaa.busy.qname "%aaa.pfx;busy">
<!ENTITY % aaa.checked.qname "%aaa.pfx;checked">
<!ENTITY % aaa.disabled.qname "%aaa.pfx;disabled">
<!ENTITY % aaa.expanded.qname "%aaa.pfx;expanded">
<!ENTITY % aaa.grab.qname "%aaa.pfx;grab">
<!ENTITY % aaa.hidden.qname "%aaa.pfx;hidden">
<!ENTITY % aaa.invalid.qname "%aaa.pfx;invalid">
<!ENTITY % aaa.pressed.qname "%aaa.pfx;pressed">
<!ENTITY % aaa.selected.qname "%aaa.pfx;selected">

<!-- properties -->

<!ENTITY % aaa.activedescendent.qname "%aaa.pfx;activedescendent">
<!ENTITY % aaa.atomic.qname "%aaa.pfx;atomic">
<!ENTITY % aaa.controls.qname "%aaa.pfx;controls">
<!ENTITY % aaa.datatype.qname "%aaa.pfx;datatype">
<!ENTITY % aaa.describedby.qname "%aaa.pfx;describedby">
<!ENTITY % aaa.dropeffect.qname "%aaa.pfx;dropeffect">
<!ENTITY % aaa.flowto.qname "%aaa.pfx;flowto">
<!ENTITY % aaa.haspopup.qname "%aaa.pfx;haspopup">
<!ENTITY % aaa.labelledby.qname "%aaa.pfx;labelledby">
<!ENTITY % aaa.level.qname "%aaa.pfx;level">
<!ENTITY % aaa.live.qname "%aaa.pfx;live">
<!ENTITY % aaa.multiselectable.qname "%aaa.pfx;multiselectable">
<!ENTITY % aaa.owns.qname "%aaa.pfx;owns">
<!ENTITY % aaa.posinset.qname "%aaa.pfx;posinset">
<!ENTITY % aaa.readonly.qname "%aaa.pfx;readonly">
<!ENTITY % aaa.relevant.qname "%aaa.pfx;relevant">
<!ENTITY % aaa.required.qname "%aaa.pfx;required">
<!ENTITY % aaa.setsize.qname "%aaa.pfx;setsize">
<!ENTITY % aaa.sort.qname "%aaa.pfx;sort">
<!ENTITY % aaa.valuemax.qname "%aaa.pfx;valuemax">
<!ENTITY % aaa.valuemin.qname "%aaa.pfx;valuemin">
<!ENTITY % aaa.valuenow.qname "%aaa.pfx;valuenow">

<!-- tabindex -->

<!ENTITY % aaa.tabindex.qname "tabindex">

<!-- The following defines a PE for use in the attribute sets of elements in
	other namespaces that want to incorporate the XML Role attributes. Note
	that in this case the XML-ROLES.pfx should always be defined. -->

<!ENTITY % aaa.states.qname "
	%aaa.busy.qname; ( true | false | error ) 'false'
	%aaa.checked.qname; (true | false | mixed) #IMPLIED
	%aaa.disabled.qname; (true | false) #IMPLIED
	%aaa.expanded.qname; (true | false | undefined) #IMPLIED
	%aaa.grab.qname; ( true | supported | false ) 'false'
	%aaa.hidden.qname; ( true | false ) 'false'
	%aaa.invalid.qname; (true | false) #IMPLIED
	%aaa.pressed.qname; (true | false ) #IMPLIED
	%aaa.selected.qname; (true | false | undefined) #IMPLIED
">

<!ENTITY % aaa.props.qname "
	%aaa.activedescendent.qname; IDREF #IMPLIED
	%aaa.atomic.qname; (true | false) 'false'
	%aaa.controls.qname; IDREFS #IMPLIED
	%aaa.datatype.qname; CDATA #IMPLIED
	%aaa.describedby.qname; IDREFS #IMPLIED
	%aaa.dropeffect.qname; ( copy  | move | reference | none ) 'none'
	%aaa.flowto.qname; IDREF #IMPLIED
	%aaa.haspopup.qname; ( true | false )  'false'
	%aaa.labelledby.qname; IDREFS #IMPLIED
	%aaa.level.qname; %Number.datatype; #IMPLIED
	%aaa.live.qname; (off | polite | assertive | rude) 'off'
	%aaa.multiselectable.qname; (true | false) #IMPLIED
	%aaa.owns.qname; IDREFS #IMPLIED
	%aaa.posinset.qname; %Number.datatype; #IMPLIED
	%aaa.readonly.qname; (true | false) #IMPLIED
	%aaa.relevant.qname; (additions | removals | text | all) #IMPLIED
	%aaa.required.qname; (true | false) #IMPLIED
	%aaa.setsize.qname; %Number.datatype; #IMPLIED
	%aaa.sort.qname; (ascending | descending) #IMPLIED
	%aaa.valuemax.qname; CDATA #IMPLIED
	%aaa.valuemin.qname; CDATA #IMPLIED
	%aaa.valuenow.qname; CDATA #IMPLIED
">

<!ENTITY % aaa.extra.attrs.qname
	"%aaa.tabindex.qname;   %Number.datatype;   #IMPLIED"
>

<!ENTITY % aaa.attrs.qname
     "%aaa.states.qname;
      %aaa.props.qname; 
       %aaa.extra.attrs.qname;"
>

<!ENTITY % aaa-qname.module "IGNORE" >

<!-- End aaa-qname Module ................................................... -->
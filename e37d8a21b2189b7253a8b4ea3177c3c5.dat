<!-- ...................................................................... -->
<!-- XHTML 1.0 Transitional Text Markup Module  ........................... -->
<!-- file: XHTML1-model-t.mod

     This is XHTML 1.0, an XML reformulation of HTML 4.0.
     Copyright 1999 Sun Microsystems, Inc., All rights reserved.
     $Id: @(#)XHTML1-model-t.mod 1.7 99/02/02 SMI $

     This DTD module is identified by the PUBLIC and SYSTEM identifiers:

     PUBLIC "-//Sun Microsystems//ELEMENTS XHTML 1.0 
                                           Transitional Document Model//EN"
     SYSTEM "XHTML1-model-t.mod"

     Revisions:
# 1999-01-14  rearranged forms and frames PEs, adding %Blkform.class;
     ....................................................................... -->

<!-- This modules declares entities describing all text flow elements,
     excluding Transitional elements. This module describes the 
     groupings of elements that make up HTML's document model. 

     HTML has two basic content models:

         %Inline.mix;  character-level elements 
         %Block.mix;   block-like elements, eg., paragraphs and lists

     The reserved word '#PCDATA' (indicating a text string) is now 
     included explicitly with each element declaration, as XML requires 
     that the reserved word occur first in a content model specification..
-->

<!-- .................  Miscellaneous Elements  ................ -->

<!-- These elements are neither block nor inline, and can 
     essentially be used anywhere in the document body -->

<!ENTITY % Misc.class 
       "ins | del | script | noscript" > 

<!-- ....................  Inline Elements  .................... -->

<!ENTITY % Inlstruct.class
       "bdo | br | span" >

<!ENTITY % Inlpres.class
       "tt | i | b | u | s | strike | big | small | font | basefont | sub | sup" >

<!ENTITY % Inlphras.class 
       "em | strong | dfn | code | samp | kbd | var | cite | abbr | acronym | q" >

<![%XHTML1-frames.module;[
<!-- %Inlspecial.class; includes iframe in Frameset DTD version -->

<!ENTITY % Inlspecial.class  "a | img | applet | object | map | iframe">
]]>

<!ENTITY % Inlspecial.class  "a | img | applet | object | map">

<!ENTITY % Formctrl.class  "input | select | textarea | label | button">

<!-- %Inline.class; includes all inline elements, used as a component in mixes -->

<!ENTITY % Inline.class  
     "%Inlstruct.class; 
      | %Inlpres.class; 
      | %Inlphras.class; 
      | %Inlspecial.class; 
      | %Formctrl.class;"
>

<!-- %Inline.mix; includes all inline elements, including %Misc.class; -->

<!ENTITY % Inline.mix  
     "%Inline.class; 
      | %Misc.class;"
>

<!-- %Inline-noa.class; includes all non-anchor inlines,
     used as a component in mixes -->

<!ENTITY % Inline-noa.class  
     "%Inlstruct.class; 
      | %Inlpres.class; 
      | %Inlphras.class; 
      | img | applet | object | map 
      | %Formctrl.class;"
>

<!-- %Inline-noa.mix; includes all non-anchor inlines -->

<!ENTITY % Inline-noa.mix  
     "%Inline-noa.class; 
      | %Misc.class;"
>

<!-- .....................  Block Elements  .................... -->

<!-- In the HTML 4.0 DTD, %heading; and %list; were included in %block;.
     These parameter entities must now be included explicitly on element
     declarations.
-->

<!--  There are six levels of headings from h1 (the most important)
      to h6 (the least important).
-->
<!ENTITY % Heading.class  "h1 | h2 | h3 | h4 | h5 | h6"> 

<!ENTITY % List.class  "ul | ol | dl | menu | dir" >

<!ENTITY % Blkstruct.class  "p | div" >

<!ENTITY % Blkpres.class  "center | hr" >

<!ENTITY % Blkphras.class  "pre | blockquote | address" >

<!ENTITY % Blkform.class  "form | fieldset" >

<![%XHTML1-frames.module;[
<!-- Blkspecial.class includes noframes in Frameset DTD version -->

<!ENTITY % Blkspecial.class  "noframes | table" >
]]>

<!ENTITY % Blkspecial.class  "table" >

<!-- HTML 4.0 Transitional legacy:
  ENTITY % block
     "P | %heading; | %list; | %preformatted; | DL | DIV | NOSCRIPT |
        BLOCKQUOTE | FORM | HR | TABLE | FIELDSET | ADDRESS"
-->

<!-- %Block.class; includes all block elements, 
     used as an component in mixes -->

<!ENTITY % Block.class 
     "%Blkstruct.class; 
      | %Blkpres.class; 
      | %Blkphras.class; 
      | %Blkform.class;
      | %Blkspecial.class;"
>

<!-- %Block.mix; includes all block elements plus %Misc.class; -->

<!ENTITY % Block.mix
     "%Block.class;
      | %Misc.class;"
> 

<!-- %Block-noform.class; includes all non-form block elements,
     used as a component in mixes -->

<!ENTITY % Block-noform.class 
     "%Blkstruct.class; 
      | %Blkpres.class; 
      | %Blkphras.class; 
      | %Blkspecial.class;"
>

<!-- %Block-noform.mix; includes all non-form block elements,
     plus %Misc.class; -->

<!ENTITY % Block-noform.mix 
     "%Block-noform.class;
      | %Misc.class;"
>

<!-- ................  All Content Elements  .................. -->

<!-- %Flow.mix; includes all text content, block and inline -->

<!ENTITY % Flow.mix
       "%Heading.class; 
      | %List.class; 
      | %Block.class;
      | %Inline.class;
      | %Misc.class;"
>

<!-- end of XHTML1-model-t.mod -->

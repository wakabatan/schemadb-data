#!/usr/bin/perl 
use strict;
use File::Find;
use Getopt::Long;
use Pod::Usage;
our $VERSION = do{my @r=(q$Revision: 1.1 $=~/\d+/g);sprintf "%d."."%02d" x $#r,@r};

my $dir = q(.);
my $filter = q/\.txt$/;
my $listfile;
my $oldlistfile;
my $newlistfile;

GetOptions (
  q(target-dir=s) => \$dir,
  q(target-filter=s) => \$filter,
  q(list-file=s) => \$listfile,
  q(removed-list-file=s) => \$oldlistfile,
  q(added-list-file=s) => \$newlistfile,
  q(h|help|?) => sub {
    pod2usage (-exitstatus => 0, -verbose => 2);
  },
) or pod2usage (-exitstatus => 2, -verbose => 1);
pod2usage (-exitstatus => 2, -verbose => 1)
  unless $listfile and $oldlistfile and $newlistfile;

my %newlist;
find (sub {
  if (/$filter/o) {
    $newlist{$File::Find::name} = 1;
  }
}, $dir);
my %list = %newlist;

my %oldlist;
if (open my $oldlist, '<', $listfile) {
  while (<$oldlist>) {
    tr/\x0A\x0D//d;
    $oldlist{$_} = 1;
  }
} else {
  warn "$0: $listfile: $!";
}

delete $newlist{$_} for keys %oldlist;
delete $oldlist{$_} for keys %list;

{
open my $list, '>', $listfile or die "$0: $listfile: $!";
  print $list $_, "\n" for sort keys %list;
}

{
open my $newlist, '>', $newlistfile or die "$0: $newlistfile: $!";
  print $newlist $_, "\n" for sort keys %newlist;
}
{
open my $oldlist, '>', $oldlistfile or die "$0: $oldlistfile: $!";
  print $oldlist $_, "\n" for sort keys %oldlist;
}

=head1 NAME

difffl.pl - Find difference between previous snapshot of and current file list

=head1 SYNOPSIS

  perl difffl.pl --list-file=file --added-list-file=file --removed-list-file=file [OPTIONS]

=head1 DESCRIPTION

Find difference between previous snapshot of file list and current file
list of certain directory (and its sub-directories).  Lists for removed files
(ie. files found in previous list but does not in current list) and
added files (ie. files not in previous list but in current list) are
outputed as separate files and file list snapshot file is updated.

=head1 OPTIONS

=over 4

=item --added-list-file=file (REQUIRED)

Path to added-file-list file.

=item --help

Show help message.

=item --list-file=file (REQUIRED)

Path to file-list file.  This file should contain previous snapshot
of file-list for target directory and is updated to latest file-list.

=item --removed-list-file=file (REQUIRED)

Path to removed-file-list file.

=item --target-dir=directory (Default: C<.>)

Path to directory from which files are to be listed.

=item --target-filter=regex (Default: C<\.txt$>)

Regular expression in Perl format, that filters files found in target
directory by their full path name.

=back

=head1 LICENSE

Copyright 2004 Wakaba <w@suika.fam.cx>.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

=cut

1; # $Date: 2004/03/13 03:07:46 $

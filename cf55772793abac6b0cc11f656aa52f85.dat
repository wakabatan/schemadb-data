<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en">

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=x-sjis">

<TITLE>XHTML 1.1 解説
</TITLE>

<LINK rel="STYLESHEET" href="style/default.css" type="text/css">
</HEAD>

<BODY>

<!--(原案改訂2003-07-12)-->
<pre>

</pre>

<center>
<h3>標準情報　TR X 0080:2003</h3>
<h2>
XHTML 1.1 - モジュールに基づくXHTML 解説</h2>
</center><br><br>

<H3>1. 公表の趣旨及び経緯</H3>
<p>
XHTML文書は, XMLに適合するため, 標準的なXMLツールを使用することによって容易に閲覧・編集が可能となり, その妥当性の検証も可能である。XMLに基づく利用者エージェントと結びついて機能することは, 今後のWeb環境での要求に適合することであり, 今後の利用者を充足させるために不可欠である。
<p>
XMLを導入することによって, 名前空間を用いて他のタグセットによる記述の導入を可能にし, さらにそれに基づくモジュール化を可能にする。XHTMLのモジュール化は, W3Cで検討されている"Modularization of XHTML"(国内では, <b>TR X 0056</b>:2002<sup>[<a href="#ref1"><b>1</b></a>]</sup>)によって規定され, その規定がXHTMLのサブセット化と拡張との実装を容易にする。つまり, ハンドヘルド装置, 携帯電話などのようにXHTML要素のサブセットだけをサポートすればよい簡素なアプリケーション, 追加要素の導入を必要とする大規模アプリケーションなどに対して, 一貫した方法で容易に対応が可能になる。文書プロファイルへの適合性は，相互運用性の保証のための基礎を提供する。
<p>
XHTML 1.0の勧告"XHTML 1.0: The Extensible HyperText Markup Language"は, 2000年1月に発表され, 国内では<b>TR X 0037</b>:2001<sup>[<a href="#ref2"><b>2</b></a>]</sup>として公表されて, HTML 4によって定義されたDTDに対応する三つのDTDを定義した。その後, 推奨しないこれまでの機能を完全に分離して一貫した将来を見据えた文書型を提供するものとして, "XHTML 1.1 - Module-based XHTML"が検討され, 2001年5月にW3Cからその勧告が発表された。
<p>
そこでINSTACの"文書処理及びフォントの標準化調査研究委員会(DDFD)"は, その2001年度の活動としてXHTML 1.1の調査研究を行い, XHTML 1.1のTR化の意義を報告した。DDFDではその作業グループ(DDFD-WG1)が, 2002年度にXHTML 1.1のTR化の作業を担当し, 2002年11月に標準情報(TR)の原案を完成して, 経済産業省の産業技術環境局に提出した。
</p>

<br>
<H3>2. 審議中の主要検討課題</H3>
<h4>2.1 訳語</h4>
<P>
訳語選定に際しては，標準情報 <b>TR X 0033</b>:2000 ハイパテキストマーク付け言語 (HTML) 4.0<sup><a href="#ref3">[<b>3</b>]</a></sup>, <b>JIS X 4156</b>:2000 ハイパテキストマーク付け言語(HTML)<sup><a href="#ref4">[<b>4</b>]</a></sup>, 及びSGML関連規格において使用している訳語との整合を配慮した。
<p>
この規格で採用した主な訳語を<b>表2.1</b>に示し, 今後の関連規定の作成等に際しての参考とする。
<p>
<table border align="center">
<caption>
<b>解説表2.1 主な訳語</b>
</caption>
<tr>
<th>原語</th>
<th>訳語</th>
<th>備考</th><tr><td>
collection of elements<td>要素の集まり<td><tr><td>
conformance definition<td>適合性定義<td>TR X 0056の訳語<tr><td>
consistently portable<td>矛盾なく移植可能な<td><tr><td>
malformed<td>乱れた形式(malformed)の<td><tr><td>
namespace designator<td>名前空間指定子<td><tr><td>
ruby annotation<td>ルビ注記<td><tr><td>
SGML open catalog entry<td>SGML開放型カタログエントリ<td>TR X 0051の訳語<tr><td>
strictly conforming<td>厳密適合の<td><tr><td>
well formed<td>整形式の<td><tr><td>
XHTML Family document type<td>XHTMLファミリ文書型<td><tr><td>
XHTML Strict<td>XHTML Strict<td>TR X 0037の訳語</tr>
</table>
</p>


<h4>2.2 章・節などの構成</h4>
<p>
W3Cの規定は, 必ずしもJIS又は標準情報(TR)の様式には整合していないため, 整合化の対応が必要である。しかしTRの読者が原規定を参照する際の便を考慮すると, 章・節構成はなるべく原規定のそれを保存することが望まれる。そこで, 次に示すだけの修正(章・節番号の変更なし)を施して, この標準情報(TR)を構成した。
<p>
<ul>
<li><b>a</b>) "Status of this document", "Versions"などをまとめて, "<b>まえがき</b>"(目次の前)とする。
<li><b>b</b>) "Abstract"を"<b>0. 適用範囲</b>"とする。
<li><b>c</b>) "<b>0. 適用範囲</b>"の前に, "<b>序文</b>"を追加する。
<li><b>d</b>) 附属書の後に, "<b>解説</b>"を追加する。
</ul>
</p>

<h4>2.3 その他の翻訳表記上の規則</h4>
<p>
原規定は, HTMLを用いて記述されている。この標準情報(TR)も原則として, 原規定のタグを保存することにしたが, 特に次の点に留意した。
<p>
<ul>
<li><b>a</b>) <b>2.</b>, <b>附属書B</b>, <b>附属書C</b>の先頭に示された章毎の目次を削除する。
<li><b>b</b>) 章の先頭のnormative/informativeの指示を削除する。
</ul>
</p>

<h4>2.4 W3Cからの要求</h4>

<p>
この標準情報(TR)の公表に際して, W3Cから和文及び英文による次の記載を求められている。この記述は原規定にはないため, ここに示して, W3Cの要求に応えることとする。

<blockquote>
<p>規定に準拠しているかどうかの基準となる版は, W3Cのサイトにある原規定とする。

<p>この標準情報(TR)は原規定と技術的に同一であることを意図しているが, 翻訳上の誤りはあり得る。
</blockquote>

<blockquote>
<p>The normative version of the specification is the English version found at the W3C site.

<p>Although this TR is intended to be technically identical to the original, it may contain errors from the translation.
</blockquote>
</p>

<br>
<H3>3. 懸案事項</H3>
<p>
翻訳作業の過程で原規定における問題点の幾つかが明らかになっている。W3Cでの次版への検討のために, これらの問題点をW3Cに提出する。
</p>

<br>
<h3>4. 参考文献</h3>
<p>
<a name=ref1>[<b>1</b>]</a> <b>TR X 0056</b>:2002 XHTMLのモジュール化, 2002-06
<p>
<a name=ref2>[<b>2</b>]</a> <b>TR X 0037</b>:2001 拡張可能なハイパテキストマーク付け言語 XHTML 1.0, 2001-02
<p>
<a name=ref3>[<b>3</b>]</a> <b>TR X 0033</b>:2002 ハイパテキストマーク付け言語 (HTML) 4.0, 2002-09
<p>
<a name=ref4>[<b>4</b>]</a> <b>JIS X 4156</b>:2000 ハイパテキストマーク付け言語(HTML), 2000-10 
</p>

<br>
<h3>5. 原案作成委員会</h3>
<p>
原案作成委員会である(財)日本規格協会 情報技術標準化研究センター(INSTAC)の"文書処理及びフォントの標準化調査研究委員会"(DDFD)は, 学識経験者, メーカ及び利用者で構成され, その委員会の中に実際の翻訳作業を行う<font -color="red">作業グループ1(DDFD-WG1)</font>が設置されている。それらの構成員を<b>解説表5.1</b>及び<b>解説表5.2</b>に示す。
<p>
<Table border align="center">
<caption>
<b><font -color="red">解説表5.1 文書処理及びフォントの標準化調査研究委員会 構成表</font></b>
</caption>
<TR VALIGN="bottom">
<th>
</th>
<th>
氏名</th>
<th>
所属</th>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(委員長)</TD>
<TD ALIGN="left">
池田 克夫</TD>
<TD ALIGN="left">
大阪工業大学</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(幹事)</TD>
<TD ALIGN="left">
鯵坂 恒夫</TD>
<TD ALIGN="left">
和歌山大学</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(幹事)</TD>
<TD ALIGN="left">
小町 祐史</TD>
<TD ALIGN="left">
パナソニックコミュニケーションズ株式会社 (SC34専門委員会委員長)</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
安達 淳</TD>
<TD ALIGN="left">
株式会社沖データ</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
内山 光一</TD>
<TD ALIGN="left">
株式会社東芝</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
小笠原 治</TD>
<TD ALIGN="left">
社団法人日本印刷技術協会</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
高沢 通</TD>
<TD ALIGN="left">
大日本スクリーン株式会社</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
高橋 亨</TD>
<TD ALIGN="left">
株式会社日立製作所</TD>
</TR>
<!--
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
加藤 幸司</TD>
<TD ALIGN="left">
凸版印刷株式会社</TD>
</TR>
-->
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
岩田 悟志</TD>
<TD ALIGN="left">
経済産業省商務情報政策局</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
木戸 達雄</TD>
<TD ALIGN="left">
経済産業省産業技術環境局</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
大久保 彰徳</TD>
<TD ALIGN="left">
株式会社リコー</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
宮本 義昭</TD>
<TD ALIGN="left">
日本ユニシス株式会社</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(オブザーバ)</TD>
<TD ALIGN="left">
高橋 昌行</TD>
<TD ALIGN="left">
経済産業省産業技術環境局</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(事務局)</TD>
<TD ALIGN="left">
内藤 昌幸</TD>
<TD ALIGN="left">
財団法人日本規格協会</TD>
</TR>

</Table>

<br>
<p>

<Table border align="center">
<caption>
<b><font -color="red">解説表5.2 作業グループ1(DDFD-WG1) 構成表</font></b>
</caption>
<TR VALIGN="bottom">
<th>
</th>
<th>
氏名</th>
<th>
所属</th>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(主査)</TD>
<TD ALIGN="left">
小町 祐史</TD>
<TD ALIGN="left">
パナソニックコミュニケーションズ株式会社 (SC34専門委員会委員長)</TD>
</TR>
<TR VALIGN="bottom">
<TD ALIGN="left">
(幹事)</TD>
<TD ALIGN="left">
内山 光一</TD>
<TD ALIGN="left">
株式会社東芝</TD>
</TR>
<TR VALIGN="bottom">
<TD ALIGN="left">
(幹事)</TD>
<TD ALIGN="left">
高橋 亨</TD>
<TD ALIGN="left">
株式会社日立製作所</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
安達 淳</TD>
<TD ALIGN="left">
株式会社沖データ</TD>
</TR>
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
今郷 詔</TD>
<TD ALIGN="left">
株式会社リコー</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
小笠原 治</TD>
<TD ALIGN="left">
社団法人日本印刷技術協会</TD>
</TR>
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
奥井 康弘</TD>
<TD ALIGN="left">
株式会社日本ユニテック</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
内藤 求</TD>
<TD ALIGN="left">
株式会社シナジー<font -color="red">･</font>インキュベート</TD>
</TR>
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
内藤 広志</TD>
<TD ALIGN="left">
大阪工業大学</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
野口 高成</TD>
<TD ALIGN="left">
ネクストソリューション株式会社</TD>
</TR>
<!--
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
萩野 剛二郎</TD>
<TD ALIGN="left">
電気通信大学</TD>
</TR>
-->
<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
渡部 賢一</TD>
<TD ALIGN="left">
財団法人日本規格協会</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(オブザーバ)</TD>
<TD ALIGN="left">
浅利 千鶴</TD>
<TD ALIGN="left">
浅利会計事務所</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="right">
</TD>
<TD ALIGN="left">
大久保 彰徳</TD>
<TD ALIGN="left">
株式会社リコー</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
</TD>
<TD ALIGN="left">
高橋 昌行</TD>
<TD ALIGN="left">
経済産業省産業技術環境局</TD>
</TR>

<TR VALIGN="bottom">
<TD ALIGN="left">
(事務局)</TD>
<TD ALIGN="left">
内藤 昌幸</TD>
<TD ALIGN="left">
財団法人日本規格協会</TD>
</TR>

</table>
</p>


<p></p>

</body>
</html>




